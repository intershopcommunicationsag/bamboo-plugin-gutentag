package com.intershop.bamboo.plugins.gutentag;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.atlassian.bamboo.ServerLifecycleProvider;
import com.atlassian.bamboo.ServerLifecycleState;
import com.atlassian.bamboo.build.pipeline.concurrent.SystemAuthorityThreadFactory;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plugin.BambooPluginUtils;
import com.intershop.bamboo.plugins.gutentag.generic.RepositoryNotAvailableException;
import com.intershop.bamboo.plugins.gutentag.generic.TagBuildInfo;
import com.intershop.bamboo.plugins.gutentag.generic.VcsTag;

class TagDetector implements Runnable {
    private static final Logger log = Logger.getLogger(TagDetector.class);

    static final long DEFAULT_INTERVAL = 300000L;
    static final int DEFAULT_MAXAGE_DAYS = 14;
    static final boolean DEFAULT_FLAG_DELETE_REMOVED = true;
    static final boolean DEFAULT_FLAG_DELETE_OLD = true;

    private final DateFormat dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.DEFAULT);
    private final ServerLifecycleProvider serverLifecycleProvider;
    private final TagBuildManager tagBuildManager;
    private final TagBuildPersistenceManager tagBuildPersistenceManager;

    private static final long INTERVAL_MIN = 60000L;
    private boolean stopRequested = false;

    private Thread tagDetectionThread;
    private long interval;
    private int maxAge;
    private boolean deleteRemoved;
    private boolean deleteOld;

    private TagDetector(long interval,
                        boolean deleteRemoved,
                        boolean deleteOld,
                        int maxAge,
                        ServerLifecycleProvider serverLifecycleProvider,
                        TagBuildManager tagBuildManager) {
        this.serverLifecycleProvider = serverLifecycleProvider;
        this.tagBuildManager = tagBuildManager;
        this.tagBuildPersistenceManager = tagBuildManager.getTagBuildPersistenceManager();
        setDeleteRemoved(deleteRemoved);
        setDeleteOld(deleteOld);
        setInterval(interval);
        setMaxAge(maxAge);
    }

    /* Runs in thread loop */
    private final BambooPluginUtils.Runnable detectionLoop = new BambooPluginUtils.Runnable("An unexpected error has occurred while detecting tags") {
        @Override
        public void run() {
            log.debug("Tag Detection Thread Loop " + tagDetectionThread.getId());
            List<PlanKey> planKeysToCheck = tagBuildPersistenceManager.getPlanKeysToCheck();
            for (PlanKey planKey : planKeysToCheck) {
                if (stopRequested) return;
                log.debug("\t" + planKey);
                scanTags(planKey);
            }
        }
    };

    private void scanTags(PlanKey planKey) {
        if (!tagBuildManager.isValidPlan(planKey)) {
            log.warn("\t\tRIP " + planKey);
            tagBuildPersistenceManager.forget(planKey);
            return;
        }

        String regex = tagBuildPersistenceManager.getRegexForPlan(planKey);

        List<VcsTag> tagsForRepo;

        try {
            tagsForRepo = tagBuildManager.getTagsForPlan(planKey);
        } catch (RepositoryNotAvailableException e) {
            log.error(e.getMessage());
            return;
        }

        List<TagBuildInfo> tagBuildInfos = tagBuildPersistenceManager.getTagsForPlan(planKey);
        List<TagBuildInfo> tagPlansToRemove = new ArrayList<>(tagBuildInfos);

        //Remove all build infos of already deleted plans
        //tagPlansToRemove.removeIf(tagBuildInfo -> !tagBuildInfo.hasPlanKey());

        List<TagBuildInfo> tagPlansOld = new ArrayList<>();

        if (tagsForRepo.isEmpty() && tagBuildInfos.isEmpty())
            return;

        if (!tagsForRepo.isEmpty()) {
            for (VcsTag vcsTag : tagsForRepo) {
                String vcsTagName = vcsTag.getName();

                Optional<TagBuildInfo> tagBuild = tagBuildInfos
                        .stream()
                        .filter(tagBuildInfo -> tagBuildInfo.getName().equals(vcsTagName))
                        .findAny();

                if ((regex.isEmpty() || vcsTagName.matches(regex)) && !tagBuild.isPresent()) {
                    log.info("\t\t" + vcsTag + " (new)");
                    tagBuildManager.buildTagForPlan(vcsTag, planKey);
                } else if (tagBuild.isPresent()) {
                    TagBuildInfo tagBuildInfo = tagBuild.get();
                    Date built = tagBuildInfo.getDate();
                    String state;

                    if (tagBuildInfo.hasPlanKey()) {
                        int age = Days.daysBetween(new DateTime(built), DateTime.now()).getDays();
                        if (age > getMaxAge()) {
                            state = "old";
                            tagPlansOld.add(tagBuildInfo);
                        }
                        else
                            state = "plan exists";
                    }
                    else {
                        state = "build recorded";
                    }
                    log.debug("\t\t" + vcsTag + " (" + state + ") [" + dateFormat.format(built) + "]");
                } else
                    log.debug("\t\t" + vcsTag + " (mysterious state)");

                //Remove plan from list of plans to be removed if the associated tag still exists
                tagPlansToRemove.removeIf(tagBuildInfo -> tagBuildInfo.getName().equals(vcsTagName));
            }
            log.debug("\t  => " + tagsForRepo.size() + " tags in repository.");
        }

        //Remove tag plans of which the associated tag doesn't exist anymore
        if (!tagPlansToRemove.isEmpty()) {
            for (TagBuildInfo tagPlan : tagPlansToRemove) {
                log.debug("\t\t" + tagPlan.getName() + " (removed)");
                PlanKey tagPlanKey = tagPlan.getPlanKey();

                if (isDeleteRemoved()) {
                    if (tagPlanKey != null && tagBuildManager.isBuildIdle(tagPlanKey)) {
                        tagBuildManager.removeTagPlan(tagPlanKey, planKey,true);
                    }
                    else if (tagPlanKey == null) {
                        tagBuildPersistenceManager.removeTagForPlan(planKey, tagPlan.getName());
                    }
                    else {
                        log.debug("\t\t\tStill building!");
                    }
                }
            }
            log.debug("\t  => " + tagPlansToRemove.size() + " removed tags in bamboo.");
        }

        //Remove tag plans of old tags but keep build info
        if (!tagPlansOld.isEmpty()) {
            for (TagBuildInfo tagPlan : tagPlansOld) {
                PlanKey tagPlanKey = tagPlan.getPlanKey();
                if (isDeleteOld()  && tagBuildManager.isBuildIdle(tagPlanKey)) {
                    tagBuildManager.removeTagPlan(tagPlanKey, planKey,false);

                }
            }
            log.debug("\t\t=> " + tagPlansOld.size() + " old tags in bamboo.");
        }
    }

    @Override
    public void run() {
        log.debug("Tag Detection thread start!");
        while (!Thread.interrupted() && !stopRequested) {
            if (!isPaused()) {
                BambooPluginUtils.callUnsafeCode(detectionLoop);
            }
            else {
                log.debug("Tag Detection paused!");
            }
            try {
                Thread.sleep(getInterval());
            } catch (InterruptedException e) {
                log.debug(e.getMessage());
            }
        }
    }
    /* End thread */

    void requestThreadStop() {
        stopRequested = true;
        log.debug("Stopping thread!");
        if (tagDetectionThread.isAlive()) {
            log.debug("Thread still alive!");
            tagDetectionThread.notify();
            try {
                tagDetectionThread.join();
            } catch (InterruptedException e) {
                log.error(e);
            } finally {
                tagDetectionThread.interrupt();
            }
            log.debug("Thread closed successfully");
        }
    }

    private void setTagDetectionThread(Thread tagDetectionThread) {
        this.tagDetectionThread = tagDetectionThread;
    }

    long getInterval() {
        return interval;
    }

    void setInterval(long interval) {
        if (interval < INTERVAL_MIN) {
            log.warn("Cannot set interval to " + interval + ". Setting to minimum (" + INTERVAL_MIN + ") instead.");
            interval = INTERVAL_MIN;
        }
        this.interval = interval;
        if (tagDetectionThread != null)
            tagDetectionThread.interrupt();
    }

    static TagDetector createInThread(long interval,
                                      boolean deleteRemoved,
                                      boolean deleteOld,
                                      int maxAge,
                                      ServerLifecycleProvider serverLifecycleProvider,
                                      TagBuildManager tagBuildManager) {
        final SystemAuthorityThreadFactory threadFactory = new SystemAuthorityThreadFactory("TagDetectionBackgroundThread");
        TagDetector tagDetector = new TagDetector(interval, deleteRemoved, deleteOld, maxAge, serverLifecycleProvider, tagBuildManager);
        Thread tagDetectionThread = threadFactory.newThread(tagDetector);
        tagDetectionThread.start();
        tagDetector.setTagDetectionThread(tagDetectionThread);
        return tagDetector;
    }

    private boolean isPaused() {
        return serverLifecycleProvider.getServerLifecycleState() != ServerLifecycleState.RUNNING;
    }

    boolean isDeleteRemoved() {
        return deleteRemoved;
    }

    void setDeleteRemoved(boolean deleteRemoved) {
        this.deleteRemoved = deleteRemoved;
    }

    boolean isDeleteOld() {
        return deleteOld;
    }

    void setDeleteOld(boolean deleteOld) {
        this.deleteOld = deleteOld;
    }

    void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    int getMaxAge() {
        return maxAge;
    }
}
