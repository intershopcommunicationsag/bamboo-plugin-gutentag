package com.intershop.bamboo.plugins.gutentag.git;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LsRemoteCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.credentials.CredentialsAccessor;
import com.atlassian.bamboo.credentials.CredentialsData;
import com.atlassian.bamboo.credentials.PrivateKeyCredentials;
import com.atlassian.bamboo.credentials.SshCredentialsImpl;
import com.atlassian.bamboo.plugins.git.GitAuthenticationType;
import com.atlassian.bamboo.plugins.git.GitPasswordCredentialsSource;
import com.atlassian.bamboo.plugins.git.v2.configurator.GitConfigurationConstants;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.google.common.collect.Lists;
import com.intershop.bamboo.plugins.gutentag.generic.RepositoryNotAvailableException;
import com.intershop.bamboo.plugins.gutentag.generic.TagSupportingRepository;
import com.intershop.bamboo.plugins.gutentag.generic.VcsTag;
import com.intershop.bamboo.plugins.gutentag.git.ssh.SshConfigSessionFactory;
import com.intershop.bamboo.plugins.gutentag.git.ssh.SshCredentialProvider;

/**
 * A git repository with tag support.
 *
 * Created by René Pasold on 22.10.2015.
 */
public class TagSupportingGitRepository implements TagSupportingRepository {
    private static final Logger log = Logger.getLogger(TagSupportingGitRepository.class);

    private final CredentialsAccessor credentialsAccessor;
    protected final Map<String, String> configuration;

    public TagSupportingGitRepository(PlanRepositoryDefinition repositoryDefinition, CredentialsAccessor credentialsAccessor) {
        this.configuration = repositoryDefinition.getVcsLocation().getConfiguration();
        this.credentialsAccessor = credentialsAccessor;
    }

    @NotNull
    @Override
    public List<VcsTag> getTags() throws RepositoryNotAvailableException {
        LsRemoteCommand lsRemoteCommand = Git.lsRemoteRepository();
        String gitUrl = getGitUrl();
        lsRemoteCommand.setRemote(gitUrl);
        String authType = getAuthType();
        CredentialsProvider credentialsProvider;
        if (authType.equals(GitAuthenticationType.PASSWORD.name())) {
            String username = getUsername();
            String password = getPassword();
            credentialsProvider = new UsernamePasswordCredentialsProvider(username, password);
            lsRemoteCommand.setCredentialsProvider(credentialsProvider);
        }
        else if (authType.equals(GitAuthenticationType.SSH_KEYPAIR.name())) {
            String credentialSource = getCredentialSource();
            String key = "";
            String passphrase = "";
            if (credentialSource.equals(GitPasswordCredentialsSource.CUSTOM.name())) {
                key = getKey();
                passphrase = getPassphrase();
            }
            else if (credentialSource.equals(GitPasswordCredentialsSource.SHARED_CREDENTIALS.name())) {
                final String sharedCredentialsId = getSharedCredentialsId();
                CredentialsData credentials = credentialsAccessor.getCredentials(Long.valueOf(sharedCredentialsId));
                if (null == credentials)
                    return Collections.emptyList();
                final PrivateKeyCredentials sshCredentials = new SshCredentialsImpl(credentials);
                key = sshCredentials.getKey();
                passphrase = sshCredentials.getPassphrase();
            }

            SshSessionFactory sshSessionFactory = new SshConfigSessionFactory(key, passphrase);
            lsRemoteCommand.setTransportConfigCallback(transport -> {
                SshTransport sshTransport = (SshTransport) transport;
                sshTransport.setSshSessionFactory(sshSessionFactory);
            });
            if (passphrase != null)
                lsRemoteCommand.setCredentialsProvider(new SshCredentialProvider(passphrase));
        }

        final List<VcsTag> tags = Lists.newArrayList();
        try {
            Collection<Ref> tagsJGit = lsRemoteCommand.call();
            tagsJGit.stream().filter(ref -> ref.getName().startsWith(Constants.R_TAGS)).forEach(ref -> {
                VcsTag vcsTag = new VcsTag(ref.getObjectId().getName(), ref.getName().substring(Constants.R_TAGS.length()), isTagBuildSupported());
                tags.add(vcsTag);
            });
        } catch (GitAPIException e) {
            log.error(e);
            throw new RepositoryNotAvailableException(gitUrl, e.getMessage());
        }

        return tags;
    }

    private String getSharedCredentialsId() {
        return configuration.get(GitConfigurationConstants.REPOSITORY_GIT_SSH_SHAREDCREDENTIALS_ID);
    }

    private String getPassword() {
        return configuration.get(GitConfigurationConstants.REPOSITORY_GIT_PASSWORD);
    }

    private String getUsername() {
        return configuration.get(GitConfigurationConstants.REPOSITORY_GIT_USERNAME);
    }

    protected String getPassphrase() {
        return configuration.get(GitConfigurationConstants.REPOSITORY_GIT_SSH_PASSPHRASE);
    }

    protected String getKey() {
        return configuration.get(GitConfigurationConstants.REPOSITORY_GIT_SSH_KEY);
    }

    protected String getCredentialSource() {
        return configuration.get(GitConfigurationConstants.REPOSITORY_GIT_SSH_CREDENTIALS_SOURCE);
    }

    protected String getAuthType() {
        return configuration.get(GitConfigurationConstants.REPOSITORY_GIT_AUTHENTICATION_TYPE);
    }

    protected String getGitUrl() {
        return configuration.get(GitConfigurationConstants.REPOSITORY_GIT_REPOSITORY_URL);
    }

    @Override
    public boolean isTagBuildSupported() {
        return true;
    }

}
