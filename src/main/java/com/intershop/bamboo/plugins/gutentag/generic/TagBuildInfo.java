package com.intershop.bamboo.plugins.gutentag.generic;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.google.common.collect.ImmutableMap;

public class TagBuildInfo {
    private static final String NAME = "name";
    private static final String PLAN_KEY = "planKey";
    private static final String DATE = "date";
    private static final String CREATION_DATE = "date";
    private final String name;
    private PlanKey planKey;
    private final Date date;
    private final Date creationDate;

    public TagBuildInfo(String name, PlanKey planKey, Date date, Date creationDate) {
        this.name = name;
        this.planKey = planKey;
        this.date = date;
        this.creationDate = creationDate;
    }

    public String getName() {
        return name;
    }

    public PlanKey getPlanKey() {
        return planKey;
    }

    public boolean hasPlanKey() {
        return planKey != null;
    }

    public void removePlanKey() {
        planKey = null;
    }

    public Date getDate() {
        return date;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Map<String, String> asMap() {
        return ImmutableMap.of(NAME, name,
                               PLAN_KEY, (planKey!=null)?planKey.getKey():"",
                               DATE, (date!=null)?String.valueOf(date.getTime()):"");
    }

    public static TagBuildInfo fromMap(Map<String, String> mapTagBuildInfo) {
        String name = mapTagBuildInfo.get(NAME);
        String strPlanKey = mapTagBuildInfo.get(PLAN_KEY);
        PlanKey planKey = StringUtils.isNotEmpty(strPlanKey) ? PlanKeys.getPlanKey(strPlanKey) : null;
        String strDate = mapTagBuildInfo.get(DATE);
        String strCreationDate = mapTagBuildInfo.getOrDefault(CREATION_DATE, strDate);
        Date date = StringUtils.isNotEmpty(strDate) ? new Date(Long.parseLong(strDate)) : new Date();
        Date creationDate = StringUtils.isNotEmpty(strCreationDate) ? new Date(Long.parseLong(strCreationDate)) : new Date();
        return new TagBuildInfo(name, planKey, date, creationDate);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TagBuildInfo))
            return false;
        else if (obj == this)
            return true;

        TagBuildInfo other = (TagBuildInfo) obj;
        return name.equals(other.getName());
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return (planKey!=null)?planKey.getKey():""
                + ":"
                + name
                + "@"
                + ((date!=null)?date.toString():"NA")
                + "|"
                + ((creationDate!=null)?creationDate.toString():"NA");
    }
}
