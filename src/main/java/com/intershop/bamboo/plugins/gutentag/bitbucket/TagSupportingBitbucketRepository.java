package com.intershop.bamboo.plugins.gutentag.bitbucket;

import com.atlassian.bamboo.credentials.CredentialsAccessor;
import com.atlassian.bamboo.plugins.git.GitAuthenticationType;
import com.atlassian.bamboo.plugins.git.GitPasswordCredentialsSource;
import com.atlassian.bamboo.plugins.stash.v2.configurator.BitbucketServerConfigurationConstants;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.intershop.bamboo.plugins.gutentag.git.TagSupportingGitRepository;
import org.apache.commons.lang3.StringUtils;

public class TagSupportingBitbucketRepository extends TagSupportingGitRepository {

    public TagSupportingBitbucketRepository(PlanRepositoryDefinition repositoryDefinition, CredentialsAccessor credentialsAccessor) {
        super(repositoryDefinition, credentialsAccessor);
    }

    @Override
    protected String getPassphrase() {
        return "";
    }

    @Override
    protected String getCredentialSource() {
        return GitPasswordCredentialsSource.CUSTOM.name();
    }

    @Override
    protected String getAuthType() {
        return GitAuthenticationType.SSH_KEYPAIR.name();
    }

    @Override
    protected String getGitUrl() {
        return configuration.get(BitbucketServerConfigurationConstants.REPOSITORY_STASH_REPOSITORY_URL).trim();
    }

    @Override
    protected String getKey() {
        return StringUtils.defaultString(configuration.get(BitbucketServerConfigurationConstants.REPOSITORY_STASH_PRIVATE_KEY), "");
    }
}
