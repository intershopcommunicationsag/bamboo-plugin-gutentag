package com.intershop.bamboo.plugins.gutentag;

import static com.atlassian.bamboo.build.creation.ChainBranchCreationConstants.BRANCH_DESCRIPTION;
import static com.atlassian.bamboo.build.creation.ChainBranchCreationConstants.BRANCH_NAME;
import static com.atlassian.bamboo.build.creation.ChainBranchCreationConstants.OVERRIDING_REPOSITORY_DEFINITION;
import static com.atlassian.bamboo.build.creation.ChainBranchCreationConstants.OVERRIDING_REPOSITORY_DEFINITION_ENTITY;
import static com.atlassian.bamboo.build.creation.ChainBranchCreationConstants.PLAN_BRANCH_WORKFLOW;
import static com.atlassian.bamboo.build.creation.ChainBranchCreationConstants.PLAN_KEY_TO_CLONE;

import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.BuildDetectionAction;
import com.atlassian.bamboo.build.BuildDetectionActionFactory;
import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.atlassian.bamboo.build.creation.BuildTriggerConditionConfigHelper;
import com.atlassian.bamboo.build.creation.ChainBranchCreationService;
import com.atlassian.bamboo.build.creation.PlanCreationService;
import com.atlassian.bamboo.chains.ChainExecutionManager;
import com.atlassian.bamboo.collections.SimpleActionParametersMap;
import com.atlassian.bamboo.credentials.CredentialsAccessor;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.event.BuildConfigurationUpdatedEvent;
import com.atlassian.bamboo.exception.WebValidationException;
import com.atlassian.bamboo.fieldvalue.BuildDefinitionConverter;
import com.atlassian.bamboo.plan.ExecutionRequestResult;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanExecutionConfig;
import com.atlassian.bamboo.plan.PlanExecutionConfigImpl;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanHelper;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.PlanService;
import com.atlassian.bamboo.plan.branch.BranchIntegrationConfigurationImpl;
import com.atlassian.bamboo.plan.branch.BranchSpecificConfiguration;
import com.atlassian.bamboo.plan.branch.ChainBranch;
import com.atlassian.bamboo.plan.branch.ChainBranchIdentifier;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.plan.branch.PlanBranchWorkflow;
import com.atlassian.bamboo.plan.branch.VcsBranch;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.trigger.PlanTrigger;
import com.atlassian.bamboo.plan.trigger.TriggerManager;
import com.atlassian.bamboo.plugin.BambooPluginKeys;
import com.atlassian.bamboo.repository.RepositoryCachingFacade;
import com.atlassian.bamboo.repository.RepositoryDataEntity;
import com.atlassian.bamboo.repository.RepositoryDataEntityImpl;
import com.atlassian.bamboo.repository.RepositoryDefinitionManager;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bamboo.util.AcquisitionPolicy;
import com.atlassian.bamboo.util.CacheAwareness;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.utils.BambooCallables;
import com.atlassian.bamboo.v2.build.trigger.CustomRevisionBuildTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.atlassian.bamboo.v2.trigger.DependencyChainListener;
import com.atlassian.bamboo.v2.trigger.ManualBuildDetectionAction;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.bamboo.variable.substitutor.VariableSubstitutor;
import com.atlassian.bamboo.vcs.configuration.PartialVcsRepositoryData;
import com.atlassian.bamboo.vcs.configuration.PartialVcsRepositoryDataImpl;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.bamboo.vcs.configuration.service.RawRepositoryConfigurationXmlConverter;
import com.atlassian.bamboo.vcs.configurator.VcsBranchConfigurator;
import com.atlassian.bamboo.vcs.module.VcsRepositoryManager;
import com.atlassian.bamboo.vcs.module.VcsRepositoryModuleDescriptor;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.spring.container.LazyComponentReference;
import com.atlassian.struts.ValidationAware;
import com.atlassian.struts.ValidationAwareSupport;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.intershop.bamboo.plugins.gutentag.bitbucket.TagSupportingBitbucketRepository;
import com.intershop.bamboo.plugins.gutentag.generic.RepositoryNotAvailableException;
import com.intershop.bamboo.plugins.gutentag.generic.TagBuildInfo;
import com.intershop.bamboo.plugins.gutentag.generic.TagSupportingRepository;
import com.intershop.bamboo.plugins.gutentag.generic.VcsTag;
import com.intershop.bamboo.plugins.gutentag.git.TagSupportingGitRepository;
import com.intershop.bamboo.plugins.gutentag.svn.TagSupportingSvnRepository;

@BambooComponent
public class TagBuildManager {

    private static final Logger log = Logger.getLogger(TagBuildManager.class);

    private static final String TRIGGER_KEY = "com.intershop.bamboo.plugins.gutentag:tagDetectionTrigger";
    private static final String GUTEN_TAG = "GutenTag";

    static final String VARIABLE_TAG_BUILD = "is.tag.build";
    static final String DESCRIPTION_TAG = "Tag: ";

    private final ChainBranchCreationService chainBranchCreationService;
    private final ChainBranchManager chainBranchManager;
    private final PlanService planService;
    private final PlanManager planManager;
    private final RepositoryDefinitionManager repositoryDefinitionManager;
    private final BuildDefinitionManager buildDefinitionManager;
    private final VcsRepositoryManager vcsRepositoryManager;
    private final EventPublisher eventPublisher;
    private final TriggerManager triggerManager;
    private final PlanExecutionManager planExecutionManager;
    private final BuildDetectionActionFactory buildDetectionActionFactory;
    private final CredentialsAccessor credentialsAccessor;
    private final DeletionService deletionService;
    private final CustomVariableContext customVariableContext;
    private final RepositoryCachingFacade repositoryCachingFacade;
    private final TagBuildPersistenceManager tagBuildPersistenceManager;
    private final CachedPlanManager cachedPlanManager;
    private final ChainExecutionManager chainExecutionManager;

    //Lazy
    private final LazyComponentReference<RawRepositoryConfigurationXmlConverter> lazyRawRepositoryConfigurationXmlConverter = new LazyComponentReference<>("rawRepositoryConfigurationXmlConverter");
    private final LazyComponentReference<BuildTriggerConditionConfigHelper> lazyBuildTriggerConditionConfigHelper = new LazyComponentReference<>("buildTriggerConditionConfigHelper");
    private final LazyComponentReference<BuildDefinitionConverter> lazyBuildDefinitionConverter = new LazyComponentReference<>("buildDefinitionConverter");


    @Autowired
    public TagBuildManager(@BambooImport ChainBranchCreationService chainBranchCreationService,
                           @BambooImport ChainBranchManager chainBranchManager,
                           @BambooImport PlanService planService,
                           @BambooImport PlanManager planManager,
                           @BambooImport RepositoryDefinitionManager repositoryDefinitionManager,
                           @BambooImport BuildDefinitionManager buildDefinitionManager,
                           @BambooImport VcsRepositoryManager vcsRepositoryManager,
                           @BambooImport EventPublisher eventPublisher,
                           @BambooImport TriggerManager triggerManager,
                           @BambooImport PlanExecutionManager planExecutionManager,
                           @BambooImport BuildDetectionActionFactory buildDetectionActionFactory,
                           @BambooImport CredentialsAccessor credentialsAccessor,
                           @BambooImport DeletionService deletionService,
                           @BambooImport CustomVariableContext customVariableContext,
                           @BambooImport RepositoryCachingFacade repositoryCachingFacade,
                           TagBuildPersistenceManager tagBuildPersistenceManager,
                           @BambooImport CachedPlanManager cachedPlanManager,
                           @BambooImport ChainExecutionManager chainExecutionManager) {
        this.chainBranchCreationService = chainBranchCreationService;
        this.chainBranchManager = chainBranchManager;
        this.planService = planService;
        this.planManager = planManager;
        this.repositoryDefinitionManager = repositoryDefinitionManager;
        this.buildDefinitionManager = buildDefinitionManager;
        this.vcsRepositoryManager = vcsRepositoryManager;
        this.eventPublisher = eventPublisher;
        this.triggerManager = triggerManager;
        this.planExecutionManager = planExecutionManager;
        this.buildDetectionActionFactory = buildDetectionActionFactory;
        this.credentialsAccessor = credentialsAccessor;
        this.deletionService = deletionService;
        this.customVariableContext = customVariableContext;
        this.repositoryCachingFacade = repositoryCachingFacade;
        this.tagBuildPersistenceManager = tagBuildPersistenceManager;
        this.cachedPlanManager = cachedPlanManager;
        this.chainExecutionManager = chainExecutionManager;
    }

    synchronized ChainBranch createTagForPlan(VcsTag vcsTag, PlanKey planKey) {
        final boolean bExistsOrConflict = tagPlanExists(vcsTag.getPlanName(), planKey);
        if (!bExistsOrConflict) {
            final PlanRepositoryDefinition defaultRepositoryDefinition = getPrimaryPlanRepositoryDefinition(planKey);
            if (null == defaultRepositoryDefinition)
                return null;

            Map<String, Object> parametersMap = new HashMap<>();
            final PlanCreationService.EnablePlan enablePlan = PlanCreationService.EnablePlan.DISABLED;
            parametersMap.put(PLAN_KEY_TO_CLONE, planKey.toString());
            parametersMap.put(BRANCH_NAME, vcsTag.getPlanName());
            parametersMap.put(BRANCH_DESCRIPTION, DESCRIPTION_TAG + vcsTag.getName() + " of " + defaultRepositoryDefinition.getName());
            parametersMap.put(PLAN_BRANCH_WORKFLOW, PlanBranchWorkflow.MANUAL_WORKFLOW.getKey());
            parametersMap.put(ChainBranchCreationService.EXPIRY_OFF, true);

            String pluginKey = defaultRepositoryDefinition.getPluginKey();
            final VcsRepositoryModuleDescriptor moduleDescriptor = vcsRepositoryManager.getVcsRepositoryModuleDescriptor(pluginKey);
            if (moduleDescriptor == null) {
                log.error("Tag creation could not be performed, repository plugin not enabled");
                return null;
            }

            if (moduleDescriptor.getVcsBranchConfigurator() != null) {
                log.debug("Creating new Tag for plan " + planKey + " using " + vcsTag.getName());

                final RawRepositoryConfigurationXmlConverter rawRepositoryConfigurationXmlConverter = lazyRawRepositoryConfigurationXmlConverter.get();
                if (rawRepositoryConfigurationXmlConverter == null) {
                    log.error("No instance of " + RawRepositoryConfigurationXmlConverter.class.getCanonicalName() + " available!");
                    return null;
                }

                final VcsBranchConfigurator vcsBranchConfigurator = moduleDescriptor.getVcsBranchConfigurator();

                final VcsBranch newBranch = vcsBranchConfigurator.createVcsBranchFromName(isSVN(pluginKey) ? vcsTag.getUri() : vcsTag.getName());
                final PartialVcsRepositoryData branchRepository = PartialVcsRepositoryDataImpl.createChildWithNewBranch(defaultRepositoryDefinition,
                        newBranch,
                        vcsBranchConfigurator);

                final RepositoryDataEntity parent = repositoryDefinitionManager.getRepositoryDataEntity(defaultRepositoryDefinition.getId());
                Preconditions.checkState(parent != null, "Parent repository no longer available!");

                final RepositoryDataEntity branchRepositoryDataEntity = new RepositoryDataEntityImpl(pluginKey,
                        defaultRepositoryDefinition.getName(),
                        defaultRepositoryDefinition.getDescription(),
                        rawRepositoryConfigurationXmlConverter.asXml(branchRepository),
                        defaultRepositoryDefinition.isMarkedForDeletion(),
                        false,
                        parent);

                parametersMap.put(OVERRIDING_REPOSITORY_DEFINITION_ENTITY, branchRepositoryDataEntity);
                parametersMap.put(OVERRIDING_REPOSITORY_DEFINITION, branchRepository);
            }

            ValidationAware validationAware = new ValidationAwareSupport();
            chainBranchCreationService.validatePlan(validationAware, new BuildConfiguration(), new SimpleActionParametersMap(parametersMap));
            if (validationAware.hasErrors()) {
                StringBuilder errorMessage = new StringBuilder();
                for (Object error : validationAware.getActionErrors()) {
                    errorMessage.append(error);
                }
                for (Object o : validationAware.getFieldErrors().entrySet()) {
                    Map.Entry fieldError = (Map.Entry) o;
                    errorMessage.append(fieldError.getKey()).append(": ").append(fieldError.getValue());
                }
                throw new InvalidParameterException("Invalid parameters while trying to create new tag " + vcsTag.getName() + " for plan " + planKey+ ". Error: " + errorMessage.toString());
            }

            PlanKey tagPlanKey;
            try {
                String strTagPlanKey = chainBranchCreationService.createPlan(new BuildConfiguration(), new SimpleActionParametersMap(parametersMap), enablePlan);
                tagPlanKey = PlanKeys.getPlanKey(strTagPlanKey);
                ImmutableChain chainMaster = getChain(planKey);
                if (chainMaster != null) {
                    invalidateCacheForOpenBranches(chainMaster);
                }
                chainBranchCreationService.triggerCreationCompleteEvents(tagPlanKey);
                log.debug("Created new Tag for plan " + planKey + " using " + vcsTag.getName());
            } catch (PlanCreationDeniedException e) {
                log.error(e);
                return null;
            }

            Plan tagPlan = planManager.getPlanByKey(tagPlanKey);
            ChainBranch chainTag = Narrow.downTo(tagPlan, ChainBranch.class);
            if (null == chainTag) {
                log.error("Tag plan for " + tagPlanKey + " doesn't seam to have been created successfully.");
                return null;
            }

            removeChainBranchTriggers(chainTag);
            eventPublisher.publish(new BuildConfigurationUpdatedEvent(this, tagPlan.getPlanKey()));

            return chainTag;
        }
        return null;
    }

    PlanResultKey buildTagForPlan(VcsTag vcsTag, PlanKey planKey) {
        final boolean bExistsOrConflict = tagPlanExists(vcsTag.getPlanName(), planKey);
        if (!bExistsOrConflict) {
            ChainBranch chainTag = createTagForPlan(vcsTag, planKey);
            if (chainTag == null) {
                log.error("Could not create tag '" + vcsTag + "' for plan " + planKey);
                return null;
            }
            ImmutableChain chain = cachedPlanManager.getPlanByKey(chainTag.getPlanKey(), ImmutableChain.class);

            final PlanRepositoryDefinition defaultRepositoryDefinition = getPrimaryPlanRepositoryDefinition(planKey);
            if (null== chain || null == defaultRepositoryDefinition)
                return null;

            String pluginKey = defaultRepositoryDefinition.getPluginKey();

            Map<String, String> variables = new HashMap<>();
            variables.put(VARIABLE_TAG_BUILD, defaultRepositoryDefinition.getName() + "/" + vcsTag.getName());

            TriggerReason triggerReason;
            Map<String, String> executionParams = new HashMap<>();
            if (isGit(pluginKey) || isBitBucketServer(pluginKey)) {
                executionParams.put(ManualBuildDetectionAction.CUSTOM_REVISION_PARAMETER, vcsTag.getUri());
                triggerReason = triggerManager.getTriggerReason(CustomRevisionBuildTriggerReason.KEY,
                        ImmutableMap.of(CustomRevisionBuildTriggerReason.TRIGGER_MANUAL_USER, GUTEN_TAG,
                                CustomRevisionBuildTriggerReason.TRIGGER_CUSTOM_REVISION, vcsTag.getUri()));
            }
            else if (isSVN(pluginKey)) {
                triggerReason = triggerManager.getTriggerReason(ManualBuildTriggerReason.KEY,
                        ImmutableMap.of(ManualBuildTriggerReason.TRIGGER_MANUAL_USER, GUTEN_TAG));
            }
            else
                return null;

            PlanExecutionConfig planExecutionConfig = new PlanExecutionConfigImpl(PlanExecutionConfig.PlanExecutionType.REGULAR);
            planExecutionConfig.setChain(chain).build();

            PlanTrigger planTrigger = triggerManager.getPlanTrigger(TRIGGER_KEY);
            executionParams.put(DependencyChainListener.DEPENDENCIES_DISABLED, Boolean.TRUE.toString());

            BuildDetectionAction buildDetectionAction = buildDetectionActionFactory.createBuildDetectionActionForPluginBuildTrigger(chain,
                    planExecutionConfig,
                    triggerReason,
                    planTrigger,
                    executionParams,
                    variables);

            log.debug(String.format("Triggering %s with %s.", chain.getPlanKey(), triggerReason.getName()));

            final BambooCallables.NotThrowing<ExecutionRequestResult> startManualExecution = () -> planExecutionManager.start(chain, buildDetectionAction, AcquisitionPolicy.IMMEDIATE);
            ExecutionRequestResult executionRequestResult = CacheAwareness.withValuesOlderThanTimestampReloaded(startManualExecution, System.currentTimeMillis(), CacheAwareness.ANY);

            PlanResultKey planResultKey = executionRequestResult.getPlanResultKey();
            log.debug(String.format("Plan %s build at %s.", chain.getPlanKey(), ((planResultKey!=null)?planResultKey.toString():"[No result]")));

            tagBuildPersistenceManager.addTagForTagPlan(chainTag, vcsTag);

            return planResultKey;
        }
        else {
            log.warn(String.format("Plan branch name '%s' conflicting for chain '%s'.", vcsTag.getName(), planKey));
            ImmutableChain chain = getChain(planKey);
            if (chain != null) {
                final ChainBranchIdentifier chainBranchIdentifier = chainBranchManager.getBranchWithName(chain,vcsTag.getPlanName());
                if (chainBranchIdentifier != null)
                    log.warn("\t" + chainBranchIdentifier.getBuildName());
            }
            log.warn("Record as missing tag!");
            tagBuildPersistenceManager.addTagForPlan(planKey, vcsTag);
        }
        return null;
    }

    private void invalidateCacheForOpenBranches(final ImmutableChain chainMaster) {
        final PlanRepositoryDefinition repositoryDefinition = PlanHelper.getDefaultPlanRepositoryDefinition(chainMaster);
        if (repositoryDefinition != null) {
            final VcsRepositoryModuleDescriptor moduleDescriptor = vcsRepositoryManager.getVcsRepositoryModuleDescriptor(repositoryDefinition.getPluginKey());
            if (moduleDescriptor != null && moduleDescriptor.supportsBranchDetection()) {
                final VariableSubstitutor variableSubstitutor = customVariableContext.getVariableSubstitutorFactory().newSubstitutorForPlan(chainMaster);
                customVariableContext.withVariableSubstitutor(variableSubstitutor,
                        () -> repositoryCachingFacade.invalidate(moduleDescriptor.getBranchDetector(), repositoryDefinition));
                log.debug(String.format("Cleared branch cache for plan %s", chainMaster.getPlanKey()));
            }
        }
    }

    private void removeChainBranchTriggers(ChainBranch chainTag) {
        log.debug("Removing triggers of " + chainTag.getPlanKey() + ".");
        BuildDefinition buildDefinition = chainTag.getBuildDefinition();
        //noinspection ConstantConditions
        HierarchicalConfiguration configuration = lazyBuildDefinitionConverter.get().fromObject(buildDefinition);
        BuildConfiguration buildConfiguration = new BuildConfiguration(configuration);
        final BuildDefinition branchBuildDefinition = buildDefinitionManager.getUnmergedBuildDefinition(chainTag.getPlanKey());
        branchBuildDefinition.setBranchIntegrationConfiguration(BuildDefinitionConverter.populate(buildConfiguration, new BranchIntegrationConfigurationImpl()));

        List<TriggerDefinition> triggerDefinitions = Lists.newArrayList();
        branchBuildDefinition.setTriggerDefinitions(triggerDefinitions);

        //noinspection ConstantConditions
        lazyBuildTriggerConditionConfigHelper.get().cleanConfig(buildConfiguration);

        final BranchSpecificConfiguration branchSpecificConfiguration = BuildDefinitionConverter.populate(buildConfiguration, new BranchSpecificConfiguration());
        if (null!=branchSpecificConfiguration) {
            branchSpecificConfiguration.setBranchCleanupDisabled(true);
            branchBuildDefinition.setBranchSpecificConfiguration(branchSpecificConfiguration);
        }

        chainTag.setSuspendedFromBuilding(false);
        buildDefinitionManager.savePlanAndDefinition(chainTag, branchBuildDefinition);

        eventPublisher.publish(new BuildConfigurationUpdatedEvent(this, chainTag.getPlanKey()));
        log.debug("Removed triggers of " + chainTag.getPlanKey() + ".");
    }

    void removeTagPlan(PlanKey planKey, PlanKey masterPlanKey, boolean removeBuildInfo) {
        tagBuildPersistenceManager.removeTagForPlan(planKey, masterPlanKey, removeBuildInfo);
        ImmutablePlan plan = cachedPlanManager.getPlanByKey(planKey);
        if (plan == null) {
            log.debug("\tPlan '" + planKey + "' disappeared");
            return;
        }
        log.debug("Delete tag plan " + plan.getKey());
        deletionService.deletePlan(plan);
    }

    boolean isValidPlan(PlanKey planKey) {
        Plan plan = planManager.getPlanByKey(planKey);
        return plan != null;
    }

    List<PlanRepositoryDefinition> getPlanRepositoryDefinitions(PlanKey planKey) {
        Plan plan = planManager.getPlanByKey(planKey);
        if (plan == null) {
            log.warn("Invalid plan '" + planKey + "'");
            return null;
        }
        return repositoryDefinitionManager.getPlanRepositoryDefinitions(plan);
    }

    PlanRepositoryDefinition getPrimaryPlanRepositoryDefinition(PlanKey planKey) {
        List<PlanRepositoryDefinition> planRepositoryDefinitions = getPlanRepositoryDefinitions(planKey);
        if (planRepositoryDefinitions == null || planRepositoryDefinitions.isEmpty()) {
            log.warn("Plan '" + planKey + "' doesn't have any repositories.");
            return null;
        }
        return planRepositoryDefinitions.get(0); //Primary repository only
    }

    @NotNull
    List<VcsTag> getTagsForPlan(@NotNull PlanKey planKey) throws RepositoryNotAvailableException {
        PlanRepositoryDefinition planRepositoryDefinition = getPrimaryPlanRepositoryDefinition(planKey);
        if (planRepositoryDefinition == null)
            return Collections.emptyList();
        return getTagsForRepo(planRepositoryDefinition);
    }

    private ImmutableChain getChain(PlanKey planKey) {
        try {
            return planService.getPlan(planKey);
        } catch (WebValidationException e) {
            log.error(e);
        }
        return null;
    }

    boolean tagPlanExists(String strTagName, PlanKey planKey){
        ImmutableChain chain = getChain(planKey);
        return tagPlanExists(strTagName, chain);
    }

    boolean isTagBuilt(String strTagName, PlanKey planKey) {
        List<TagBuildInfo> tagsForPlan = tagBuildPersistenceManager.getTagsForPlan(planKey);
        return tagsForPlan.stream().anyMatch(tagBuildInfo -> tagBuildInfo.getName().equals(strTagName));
    }

    /**
     * Record all existing, currently unbuilt tags (so they're not built).
     * @param planKey the plan key.
     */
    void drawBaseline(PlanKey planKey)
    {
        List<TagBuildInfo> recordedTags = tagBuildPersistenceManager.getTagsForPlan(planKey);

        try {
            List<VcsTag> tagsForPlan = getTagsForPlan(planKey);
            for (VcsTag vcsTag : tagsForPlan) {
                if (recordedTags.stream().noneMatch(tagBuildInfo -> tagBuildInfo.getName().equals(vcsTag.getName()))) {
                    tagBuildPersistenceManager.addTagForPlan(planKey, vcsTag);
                }
            }
        } catch (RepositoryNotAvailableException e) {
            log.error(e);
        }
    }

    boolean isBuildIdle(PlanKey planKey) {
        return !chainExecutionManager.isActive(planKey);
    }

    private boolean tagPlanExists(String strTagName, ImmutableChain chain){
        return chainBranchManager.isPlanBranchNameConflicting(chain, -1, strTagName);
    }

    List<VcsTag> getTagsForRepo(@NotNull PlanRepositoryDefinition repositoryDefinition) throws RepositoryNotAvailableException {
        List<VcsTag> listVcsTags;

        TagSupportingRepository tagSupportingRepository = getTagSupportingRepository(repositoryDefinition);

        listVcsTags = tagSupportingRepository.getTags();
        listVcsTags.sort((o1, o2) -> o2.getName().compareTo(o1.getName()));

        return listVcsTags;
    }

    TagSupportingRepository getTagSupportingRepository(@NotNull PlanRepositoryDefinition repositoryDefinition) {
        String pluginKey = repositoryDefinition.getPluginKey();

        if (isSVN(pluginKey)) {
            return new TagSupportingSvnRepository(repositoryDefinition);
        } else if (isGit(pluginKey)) {
            return new TagSupportingGitRepository(repositoryDefinition, credentialsAccessor);
        } else if (isBitBucketServer(pluginKey)) {
            return new TagSupportingBitbucketRepository(repositoryDefinition, credentialsAccessor);
        } else
            log.debug(pluginKey + ": " + repositoryDefinition.getVcsLocation().getConfiguration());

        return new TagSupportingRepository() {
            @NotNull
            @Override
            public List<VcsTag> getTags() {
                return Collections.emptyList();
            }

            @Override
            public boolean isTagBuildSupported() {
                return false;
            }
        };
    }

    private static boolean isGit(String pluginKey) {
        return pluginKey.matches(BambooPluginKeys.GIT_REPOSITORY_V2_PLUGIN_KEY);
    }

    private static boolean isSVN(String pluginKey) {
        return pluginKey.matches(BambooPluginKeys.SVN_REPOSITORY_V2_PLUGIN_KEY);
    }

    private static boolean isBitBucketServer(String pluginKey) {
        return pluginKey.matches(BambooPluginKeys.BB_SERVER_REPOSITORY_PLUGIN_KEY);
    }

    TagBuildPersistenceManager getTagBuildPersistenceManager() {
        return tagBuildPersistenceManager;
    }
}
