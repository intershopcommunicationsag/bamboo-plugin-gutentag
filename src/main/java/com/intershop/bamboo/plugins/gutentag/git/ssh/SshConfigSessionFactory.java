package com.intershop.bamboo.plugins.gutentag.git.ssh;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.util.FS;

public class SshConfigSessionFactory extends JschConfigSessionFactory {
    private final String sshKey;
    private final String passphrase;

    public SshConfigSessionFactory(String sshKey, String passphrase) {
        this.sshKey = sshKey;
        this.passphrase = passphrase;
    }

    @Override
    protected void configure(OpenSshConfig.Host host, Session session) {
    }

    @Override
    protected JSch getJSch(final OpenSshConfig.Host hc, final FS fs) throws JSchException {
        final JSch jsch = super.getJSch(hc, fs);
        jsch.removeAllIdentity();
        if (StringUtils.isNotEmpty(sshKey) && passphrase != null) {
            jsch.addIdentity("identityName", sshKey.getBytes(), null, passphrase.getBytes());
        }
        return jsch;
    }
}
