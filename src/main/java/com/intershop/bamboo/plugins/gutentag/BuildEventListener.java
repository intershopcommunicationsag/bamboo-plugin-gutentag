package com.intershop.bamboo.plugins.gutentag;

import com.atlassian.bamboo.chains.ChainExecution;
import com.atlassian.bamboo.chains.ChainResultsSummary;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.execution.triggering.EnvironmentTriggeringAction;
import com.atlassian.bamboo.deployments.execution.triggering.EnvironmentTriggeringActionFactory;
import com.atlassian.bamboo.event.ChainCompletedEvent;
import com.atlassian.bamboo.event.HibernateEventListenerAspect;
import com.atlassian.bamboo.plan.*;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.trigger.TriggerableInternalKey;
import com.atlassian.bamboo.trigger.dependency.EnvironmentDependencyService;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Map;

/**
 * Listens for build plan events
 * Created by rpasold on 19.10.2015.
 */
@BambooComponent
public class BuildEventListener {
    private final EventListenerRegistrar eventListenerRegistrar;
    private final EnvironmentDependencyService environmentDependencyService;
    private final EnvironmentTriggeringActionFactory environmentTriggeringActionFactory;
    private final NonBlockingPlanExecutionService nonBlockingPlanExecutionService;
    private final PlanManager planManager;
    private final ResultsSummaryManager resultsSummaryManager;

    @Autowired
    public BuildEventListener(@BambooImport("eventPublisher") final EventListenerRegistrar eventListenerRegistrar,
                              @BambooImport final EnvironmentDependencyService environmentDependencyService,
                              @BambooImport final EnvironmentTriggeringActionFactory environmentTriggeringActionFactory,
                              @BambooImport final NonBlockingPlanExecutionService nonBlockingPlanExecutionService,
                              @BambooImport final ResultsSummaryManager resultsSummaryManager,
                              @BambooImport final PlanManager planManager) {
        this.eventListenerRegistrar = eventListenerRegistrar;
        this.environmentDependencyService = environmentDependencyService;
        this.environmentTriggeringActionFactory = environmentTriggeringActionFactory;
        this.nonBlockingPlanExecutionService = nonBlockingPlanExecutionService;
        this.resultsSummaryManager = resultsSummaryManager;
        this.planManager = planManager;
    }

    @EventListener
    @HibernateEventListenerAspect
    public void onChainCompleted(final ChainCompletedEvent chainCompletedEvent) {
        final ChainExecution chainExecution = chainCompletedEvent.getChainExecution();
        final ChainResultsSummary crs =
                Preconditions.checkNotNull(
                        resultsSummaryManager.getResultsSummary(chainExecution.getPlanResultKey(), ChainResultsSummary.class),
                        "Unable to find the chain result for " + chainExecution.getPlanResultKey()
                );

        if (chainExecution.isSuccessful() && !crs.isContinuable()) {
            PlanResultKey planResultKey = chainCompletedEvent.getPlanResultKey();
            PlanKey planKey = chainCompletedEvent.getPlanKey();
            Plan plan = planManager.getPlanByKey(planKey);

            Preconditions.checkNotNull(
                    plan,
                    "Unable to find the plan for " + chainExecution.getPlanResultKey()
            );

            if (plan.hasMaster()) {
                ImmutablePlan planMaster = plan.getMaster();
                Preconditions.checkNotNull(
                        planMaster,
                        "Unable to find the master plan for " + chainExecution.getPlanResultKey()
                );
                planKey = planMaster.getPlanKey();
            }


            Map<String, VariableDefinitionContext> mapEffectiveVariables = chainCompletedEvent.getBuildContext().getVariableContext().getEffectiveVariables();
            if (mapEffectiveVariables.containsKey(TagBuildManager.VARIABLE_TAG_BUILD)) {

                for (TriggerableInternalKey triggerableInternalKey : environmentDependencyService.getEnvironmentsToTrigger(TriggerableInternalKeyImpl.forPlanKey(planKey))) {
                    Environment environment = Narrow.downTo(triggerableInternalKey.getTriggerable(), Environment.class);
                    if (environment != null && !environment.isSuspended()) {
                        EnvironmentTriggeringAction triggeringAction = environmentTriggeringActionFactory.createAfterSuccessfulPlanEnvironmentTriggerAction(environment, planResultKey);
                        nonBlockingPlanExecutionService.tryToStart(environment, triggeringAction);
                    }
                }
            }
            //planManager.setPlanSuspendedState(plan.getPlanKey(), true);
        }
    }

    @PostConstruct
    private void postConstruct() {
        eventListenerRegistrar.register(this);
    }

    @PreDestroy
    private void preDestroy() {
        eventListenerRegistrar.unregister(this);
    }
}
