package com.intershop.bamboo.plugins.gutentag;

import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.bamboo.FeatureManager;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.event.BuildConfigurationUpdatedEvent;
import com.atlassian.bamboo.jsonator.Jsonator;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.utils.i18n.I18nBeanFactory;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.bamboo.ww2.actions.ChainActionSupport;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.bamboo.ww2.aware.BuildConfigurationAware;
import com.atlassian.bamboo.ww2.aware.permissions.PlanEditSecurityAware;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.google.common.collect.ImmutableList;

@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class ConfigureTagDetectionPlanAction extends ChainActionSupport implements BuildConfigurationAware, PlanEditSecurityAware {
    private static final long serialVersionUID = 102082072992352702L;

    private static final String TAG_NAME_PATTERN_FIELD = "planTagCreationRegularExpression";

    private final EventPublisher eventPublisher;
    private final TagBuildManager tagBuildManager;

    private BuildConfiguration buildConfiguration;
    private boolean newOnly;
    private String planTagCreation;
    private String planTagCreationRegularExpression = TagDetectionConfiguration.DEFAULT_TAG_MATCH_NAME_PATTERN;
    private boolean isPlanTagCreationEnabled;

    @Autowired
    public ConfigureTagDetectionPlanAction(@BambooImport FeatureManager featureManager,
                                           @BambooImport BambooAuthenticationContext bambooAuthenticationContext,
                                           @BambooImport WebInterfaceManager webInterfaceManager,
                                           @BambooImport BambooPermissionManager bambooPermissionManager,
                                           @BambooImport PlanExecutionManager planExecutionManager,
                                           @BambooImport PlanManager planManager,
                                           @BambooImport EventPublisher eventPublisher,
                                           @BambooImport BuildDefinitionManager buildDefinitionManager,
                                           @BambooImport I18nBeanFactory i18nBeanFactory,
                                           @BambooImport Jsonator jsonator,
                                           TagBuildManager tagBuildManager) {
        this.eventPublisher = eventPublisher;
        this.tagBuildManager = tagBuildManager;
        super.setFeatureManager(featureManager);
        super.setAuthenticationContext(bambooAuthenticationContext);
        super.setWebInterfaceManager(webInterfaceManager);
        super.setBambooPermissionManager(bambooPermissionManager);
        super.setPlanExecutionManager(planExecutionManager);
        super.setPlanManager(planManager);
        super.setBuildDefinitionManager(buildDefinitionManager);
        super.setI18nBeanFactory(i18nBeanFactory);
        super.setJsonator(jsonator);
    }

    @Override
    public void validate() {
        try {
            if (planTagCreationRegularExpression.isEmpty()) {
                addFieldError(TAG_NAME_PATTERN_FIELD, "This field cannot be empty.");
            }
            Pattern.compile(planTagCreationRegularExpression);
        } catch (PatternSyntaxException e) {
            addFieldError(TAG_NAME_PATTERN_FIELD, "Invalid expression. Please specify a valid one. " + e.toString());
        }
    }

    @Override
    public String execute() {
        ImmutablePlan plan = getImmutablePlan();
        BuildDefinition buildDefinition = plan.getBuildDefinition();
        Map<String, String> customConfiguration = buildDefinition.getCustomConfiguration();
        TagDetectionConfiguration tagDetectionConfiguration = new TagDetectionConfiguration(customConfiguration, plan.getPlanKey());
        populateTagDetectionSettings(tagDetectionConfiguration);

        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public String update() {
        final Plan plan = getMutablePlan();
        PlanKey planKey = plan.getPlanKey();
        final BuildDefinition buildDefinition = buildDefinitionManager.getBuildDefinition(planKey);
        Map<String, String> customConfiguration = buildDefinition.getCustomConfiguration();
        final TagDetectionConfiguration tagDetectionConfiguration = new TagDetectionConfiguration(customConfiguration, planKey);

        updateTagDetectionConfiguration(tagDetectionConfiguration);

        customConfiguration.putAll(tagDetectionConfiguration.toConfiguration());
        buildDefinition.setCustomConfiguration(customConfiguration);

        if (newOnly) {
            tagBuildManager.drawBaseline(planKey);
        }

        buildDefinitionManager.savePlanAndDefinition(plan, buildDefinition);
        eventPublisher.publish(new BuildConfigurationUpdatedEvent(this, planKey));

        return SUCCESS;
    }

    @Override
    public BuildConfiguration getBuildConfiguration() {
        return buildConfiguration;
    }

    @Override
    public void setBuildConfiguration(BuildConfiguration buildConfiguration) {
        this.buildConfiguration = buildConfiguration;
    }

    public boolean isTagDetectionCapable() {
        PlanRepositoryDefinition primaryPlanRepositoryDefinition = tagBuildManager.getPrimaryPlanRepositoryDefinition(getTypedPlanKey());
        return tagBuildManager.getTagSupportingRepository(primaryPlanRepositoryDefinition).isTagBuildSupported();
    }

    public ImmutableList<TagCreationType> getTagCreationTypes() {
        return ImmutableList.copyOf(TagCreationType.values());
    }

    @SuppressWarnings("unused")
    public boolean getNewOnly() {
        return newOnly;
    }

    @SuppressWarnings("unused")
    public void setNewOnly(boolean newOnly) {
        this.newOnly = newOnly;
    }

    @SuppressWarnings("unused")
    public String getPlanTagCreation() {
        return planTagCreation;
    }

    @SuppressWarnings("unused")
    public void setPlanTagCreation(String planTagCreation) {
        this.planTagCreation = planTagCreation;
    }

    @SuppressWarnings("unused")
    public String getPlanTagCreationRegularExpression() {
        return planTagCreationRegularExpression;
    }

    @SuppressWarnings("unused")
    public void setPlanTagCreationRegularExpression(String planTagCreationRegularExpression) {
        this.planTagCreationRegularExpression = planTagCreationRegularExpression;
    }

    private void updateTagDetectionConfiguration(final TagDetectionConfiguration tagDetectionConfiguration) {
        isPlanTagCreationEnabled = planTagCreation != null && !TagCreationType.DISABLED.getKey().equals(planTagCreation);
        tagDetectionConfiguration.setPlanTagCreationEnabled(isPlanTagCreationEnabled);
        if (isPlanTagCreationEnabled) {
            if (TagCreationType.NEW_MATCHES.getKey().equals(planTagCreation))
                tagDetectionConfiguration.setMatchingPattern(planTagCreationRegularExpression);
            else
                tagDetectionConfiguration.setMatchingPattern(StringUtils.EMPTY);
        }
    }

    private void populateTagDetectionSettings(TagDetectionConfiguration tagDetectionConfiguration) {
        isPlanTagCreationEnabled = tagDetectionConfiguration.isPlanTagCreationEnabled();
        if (isPlanTagCreationEnabled) {
            if (StringUtils.isEmpty(tagDetectionConfiguration.getMatchingPattern()))
                planTagCreation = TagCreationType.ALL_NEW.getKey();
            else {
                planTagCreation = TagCreationType.NEW_MATCHES.getKey();
                planTagCreationRegularExpression = tagDetectionConfiguration.getMatchingPattern();
            }
        } else
            planTagCreation = TagCreationType.DISABLED.getKey();
    }
}
