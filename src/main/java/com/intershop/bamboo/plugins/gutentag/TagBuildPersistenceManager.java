package com.intershop.bamboo.plugins.gutentag;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.exception.WebValidationException;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanService;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.plan.branch.ChainBranch;
import com.atlassian.bamboo.plan.branch.ChainBranchIdentifier;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.intershop.bamboo.plugins.gutentag.generic.TagBuildInfo;
import com.intershop.bamboo.plugins.gutentag.generic.VcsTag;

@BambooComponent
public class TagBuildPersistenceManager {

    private static final Logger log = Logger.getLogger(TagBuildPersistenceManager.class);

    private static final String PLUGIN_KEY = "com.intershop.bamboo.plugins.gutentag";
    private static final String KEY_BUILT_TAGS = "gutentag.built.tags";

    private final BandanaManager bandanaManager;
    private final PlanManager planManager;
    private final ChainBranchManager chainBranchManager;
    private final BuildDefinitionManager buildDefinitionManager;
    private final PlanService planService;

    @Autowired
    public TagBuildPersistenceManager(@BambooImport BandanaManager bandanaManager,
                                      @BambooImport PlanManager planManager,
                                      @BambooImport ChainBranchManager chainBranchManager,
                                      @BambooImport BuildDefinitionManager buildDefinitionManager,
                                      @BambooImport PlanService planService) {
        this.bandanaManager = bandanaManager;
        this.planManager = planManager;
        this.chainBranchManager = chainBranchManager;
        this.buildDefinitionManager = buildDefinitionManager;
        this.planService = planService;
    }

    void forget(@NotNull PlanKey planKey) {
        final BandanaContext context = getContext(planKey);
        if (context == null)
            return;

        bandanaManager.removeValue(context, KEY_BUILT_TAGS);
        log.debug("Forget: " + planKey);
    }

    List<TagBuildInfo> getTagsForPlan(@NotNull PlanKey planKey) {
        final BandanaContext context = getContext(planKey);
        if (context == null)
            return Collections.emptyList();

        List<TagBuildInfo> tagsForPlanBuildInfo = null;
        Object value;
        try {
             value = bandanaManager.getValue(context, KEY_BUILT_TAGS);
            @SuppressWarnings("unchecked") List<Map<String, String>> maps = (List<Map<String, String>>) value;

            if (null != maps) {
                tagsForPlanBuildInfo = new ArrayList<>();
                for (Map<String, String> map : maps) {
                    tagsForPlanBuildInfo.add(TagBuildInfo.fromMap(map));
                }
            }
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
            log.warn("Update from incompatible persistence for plan " + planKey.getKey() + "!");
        }

        if (null == tagsForPlanBuildInfo) {
            tagsForPlanBuildInfo = getExistingTagPlans(planKey);
            setTagsForPlan(planKey, tagsForPlanBuildInfo);
        }

        return tagsForPlanBuildInfo;
    }

    private void setTagsForPlan(@NotNull PlanKey planKey, List<TagBuildInfo> tagsForPlanBuildInfo) {
        final BandanaContext context = getContext(planKey);
        if (context == null)
            return;
        List<Map<String, String>> maps = new ArrayList<>();

        List<TagBuildInfo> tagBuildInfos = tagsForPlanBuildInfo.stream().distinct().collect(Collectors.toList());

        for (TagBuildInfo tagBuildInfo : tagBuildInfos) {
            maps.add(tagBuildInfo.asMap());
        }
        bandanaManager.setValue(context, KEY_BUILT_TAGS, maps);
    }

    void addTagForTagPlan(@NotNull ChainBranch chainTag, @NotNull VcsTag tag) {
        ImmutablePlan planMaster = chainTag.getMaster();
        if (planMaster == null) return;

        PlanKey masterPlanKey = planMaster.getPlanKey();

        List<TagBuildInfo> buildTagsForPlan = getTagsForPlan(masterPlanKey);
        buildTagsForPlan.add(new TagBuildInfo(tag.getName(), chainTag.getPlanKey(), new Date(), (chainTag.getCommitInformation()!=null)?chainTag.getCommitInformation().getCreatingCommitDate():null));

        setTagsForPlan(masterPlanKey, buildTagsForPlan);
    }

    void addTagForPlan(@NotNull PlanKey masterPlanKey, @NotNull VcsTag tag) {
        Plan plan = planManager.getPlanByKey(masterPlanKey);
        if (plan == null) {
            log.debug("Master plan not found " + masterPlanKey);
             return;
        }

        List<TagBuildInfo> buildTagsForPlan = getTagsForPlan(masterPlanKey);
        ChainBranch chainBranch = getExistingTagPlan(masterPlanKey, tag);

        TagBuildInfo tagBuildInfo;
        if (chainBranch != null) {
            Date creationDate = (chainBranch.getCommitInformation() != null) ? chainBranch.getCommitInformation().getCreatingCommitDate() : null;
            ResultsSummary latestResultsSummary = chainBranch.getLatestResultsSummary();
            Date buildDate = latestResultsSummary!=null?latestResultsSummary.getBuildDate():new Date();
            tagBuildInfo = new TagBuildInfo(tag.getName(), chainBranch.getPlanKey(), buildDate, creationDate);
        }
        else {
            tagBuildInfo = new TagBuildInfo(tag.getName(), null, new Date(), new Date());
        }
        buildTagsForPlan.add(tagBuildInfo);
        setTagsForPlan(masterPlanKey, buildTagsForPlan);
        log.debug("Recorded tag: " + tagBuildInfo);
    }

    void removeTagForPlan(@NotNull PlanKey planKey, @NotNull PlanKey masterPlanKey, boolean removeBuildInfo) {
        List<TagBuildInfo> buildTagsForPlan = getTagsForPlan(masterPlanKey);

        List<TagBuildInfo> tagBuildInfos = buildTagsForPlan.stream()
                .filter(TagBuildInfo::hasPlanKey)
                .filter(tagBuildInfo -> tagBuildInfo.getPlanKey().equals(planKey))
                .collect(Collectors.toList());

        if (!removeBuildInfo)
            tagBuildInfos.forEach(TagBuildInfo::removePlanKey);
        else
            buildTagsForPlan.removeAll(tagBuildInfos);

        setTagsForPlan(masterPlanKey, buildTagsForPlan);
    }

    void removeTagForPlan(@NotNull PlanKey masterPlanKey, String tagName) {
        Plan plan = planManager.getPlanByKey(masterPlanKey);
        if (plan == null) return;

        List<TagBuildInfo> buildTagsForPlan = getTagsForPlan(masterPlanKey);

        List<TagBuildInfo> builtTagsFiltered = buildTagsForPlan.stream()
                .filter(tagBuildInfo -> !tagBuildInfo.getName().equals(tagName))
                .collect(Collectors.toList());

        setTagsForPlan(masterPlanKey, builtTagsFiltered);
        log.debug("\tRemove recorded tag: " + tagName);
    }

    private BandanaContext getContext(@NotNull PlanKey planKey) {
        Plan plan = planManager.getPlanByKey(planKey);
        if (plan == null) {
            log.debug("\tInvalid plan: " + planKey);
            return null;
        }

        return PlanAwareBandanaContext.forPlugin(PlanAwareBandanaContext.forPlan(plan), PLUGIN_KEY);
    }

    private ChainBranch getExistingTagPlan(@NotNull PlanKey planKey, @NotNull VcsTag tag) {
        ImmutableChain immutablePlan = getChain(planKey);
        if (null == immutablePlan) return null;
        List<ChainBranch> branchesForChain = chainBranchManager.getBranchesForChain(immutablePlan);
        return branchesForChain.stream()
                .filter(chainBranch -> chainBranch.getBuildName().equals(tag.getPlanName()))
                .findFirst().orElse(null);
    }

    private List<TagBuildInfo> getExistingTagPlans(@NotNull PlanKey planKey) {
        log.info("Recovering tag build info from existing plans for '" + planKey +"'!");
        ImmutableChain immutablePlan = getChain(planKey);
        if (immutablePlan != null) {
            String regex = getTagDetectionConfiguration(planKey).getMatchingPattern();

            List<ChainBranch> branchesForChain = chainBranchManager.getBranchesForChain(immutablePlan);

            return branchesForChain.stream()
                    .filter(chainBranch -> isTagPlan(chainBranch, regex))
                    .map(cb -> {
                        Date date = (cb.getCommitInformation() != null) ? cb.getCommitInformation().getCreatingCommitDate() : null;
                        return new TagBuildInfo(cb.getBuildName(), cb.getPlanKey(), date, date);
                    })
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private ImmutableChain getChain(PlanKey planKey) {
        try {
            return planService.getPlan(planKey);
        } catch (WebValidationException e) {
            log.error(e);
        }
        return null;
    }

    private boolean isTagPlan(ChainBranchIdentifier chainBranchIdentifier, String regex) {
        BuildDefinition buildDefinition = buildDefinitionManager.getBuildDefinition(chainBranchIdentifier.getPlanKey());
        String name = chainBranchIdentifier.getBuildName();
        String description = chainBranchIdentifier.getDescription();

        boolean tag = description != null && !description.isEmpty() && description.startsWith(TagBuildManager.DESCRIPTION_TAG);
        boolean matches = !regex.isEmpty() && name.matches(regex);
        List<TriggerDefinition> triggerDefinitions = buildDefinition.getTriggerDefinitions();
        boolean empty = triggerDefinitions == null || triggerDefinitions.isEmpty();

        return tag || (empty && matches);
    }

    private List<TagDetectionConfiguration> getTagDetectionConfigurations() {
        List<TagDetectionConfiguration> tagDetectionConfigurations = new ArrayList<>();
        List<TopLevelPlan> allPlanKeys = planManager.getAllPlansUnrestricted();
        for (TopLevelPlan plan : allPlanKeys) {
            TagDetectionConfiguration tagDetectionConfiguration = getTagDetectionConfiguration(plan.getPlanKey());
            tagDetectionConfigurations.add(tagDetectionConfiguration);
        }
        return tagDetectionConfigurations;
    }

    private boolean cacheValid = false;
    private final List<PlanKey> planKeyCache = Lists.newArrayList();
    private final Map<PlanKey, String> planRegexCache = Maps.newHashMap();

    List<PlanKey> getPlanKeysToCheck() {
        if (!cacheValid) {
            planKeyCache.clear();
            planRegexCache.clear();
            List<TagDetectionConfiguration> tagDetectionConfigurations = getTagDetectionConfigurations();
            for (TagDetectionConfiguration tagDetectionConfiguration : tagDetectionConfigurations) {
                if (tagDetectionConfiguration.isPlanTagCreationEnabled()) {
                    PlanKey planKey = tagDetectionConfiguration.getPlanKey();
                    planKeyCache.add(planKey);
                    planRegexCache.put(planKey, tagDetectionConfiguration.getMatchingPattern());
                }
            }
            cacheValid = true;
        }
        return planKeyCache;
    }

    String getRegexForPlan(@NotNull PlanKey planKey) {
        if (null == planManager.getPlanByKey(planKey)) {
            invalidateCache();
            getPlanKeysToCheck();
        }
        return planRegexCache.get(planKey);
    }

    void invalidateCache() {
        cacheValid = false;
        log.info("Cache invalidated!");
    }

    @NotNull
    private TagDetectionConfiguration getTagDetectionConfiguration(PlanKey planKey) {
        BuildDefinition buildDefinition = buildDefinitionManager.getBuildDefinition(planKey);
        Map<String, String> customConfiguration = buildDefinition.getCustomConfiguration();
        return new TagDetectionConfiguration(customConfiguration, planKey);
    }

}
