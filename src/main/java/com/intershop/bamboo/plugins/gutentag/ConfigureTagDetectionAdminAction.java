package com.intershop.bamboo.plugins.gutentag;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.bamboo.configuration.GlobalAdminAction;

@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class ConfigureTagDetectionAdminAction extends GlobalAdminAction {

    private static final long serialVersionUID = 7228333307010992726L;

    private static final Logger log = Logger.getLogger(ConfigureTagDetectionAdminAction.class);
    private static final int TIME_SCALE = 60 * 1000;

    @Autowired private TagDetectionService tagDetectionService;

    private long interval;
    private boolean deleteRemoved;
    private boolean deleteOld;
    private int maxAge;

    private final DecimalFormat decimalFormat = new DecimalFormat("#.##");

    @SuppressWarnings("unused")
    public String getInterval() {
        interval = tagDetectionService.getTagDetector().getInterval();
        return decimalFormat.format((double)interval / TIME_SCALE);
    }

    @SuppressWarnings("unused")
    public void setInterval(String strInterval) {
        try {
            double input = Double.parseDouble(strInterval);
            interval = (long)( input * TIME_SCALE );
        }
        catch (NumberFormatException nfe) {
            log.warn(strInterval + " is not a number.");
        }
    }

    @SuppressWarnings("unused")
    public boolean isDeleteRemoved() {
        deleteRemoved = tagDetectionService.getTagDetector().isDeleteRemoved();
        return deleteRemoved;
    }

    @SuppressWarnings("unused")
    public void setDeleteRemoved(boolean deleteRemoved) {
        this.deleteRemoved = deleteRemoved;
    }

    @SuppressWarnings("unused")
    public boolean isDeleteOld() {
        deleteOld = tagDetectionService.getTagDetector().isDeleteOld();
        return deleteOld;
    }

    @SuppressWarnings("unused")
    public void setDeleteOld(boolean deleteOld) {
        this.deleteOld = deleteOld;
    }

    @SuppressWarnings("unused")
    public String getMaxAge() {
        maxAge = tagDetectionService.getTagDetector().getMaxAge();
        return String.valueOf(maxAge);
    }

    @SuppressWarnings("unused")
    public void setMaxAge(String strMaxAge) {
        try {
            maxAge = Integer.parseInt(strMaxAge);
        }
        catch (NumberFormatException nfe) {
            log.warn(strMaxAge + " is not an integer.");
        }
    }

    @SuppressWarnings("unused")
    public String doSave() {
        tagDetectionService.getTagDetector().setInterval(interval);
        tagDetectionService.saveInterval(interval);
        tagDetectionService.getTagDetector().setDeleteRemoved(deleteRemoved);
        tagDetectionService.saveDeleteRemoved(deleteRemoved);
        tagDetectionService.getTagDetector().setDeleteOld(deleteOld);
        tagDetectionService.saveDeleteOld(deleteOld);
        tagDetectionService.getTagDetector().setMaxAge(maxAge);
        tagDetectionService.saveMaxAge(maxAge);
        return SUCCESS;
    }
}
