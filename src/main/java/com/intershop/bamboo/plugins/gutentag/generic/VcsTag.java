package com.intershop.bamboo.plugins.gutentag.generic;

import com.atlassian.bamboo.plan.branch.ChainBranchUtils;

/**
 * A tag in a VCS.
 * Created by rpasold on 29.09.2015.
 */
public class VcsTag {

    private final String strURI;
    private final String strName;
    private final boolean canBuild;

    public VcsTag(String strName) {
        this.strURI = null;
        this.strName = strName;
        this.canBuild = false;
    }

    public VcsTag(String strURI, String strName, boolean canBuild) {
        this.strURI = strURI;
        this.strName = strName;
        this.canBuild = canBuild;
    }

    public String getUri() {
        return strURI;
    }

    public String getName() {
        return strName;
    }

    /**
     * @return a name for this tag that's valid as a plan name in Bamboo.
     */
    public String getPlanName() {
        //[A plan name cannot contain a [, ], {, }, <, >, :, @, /, &, %, \, !, |, #, $, *, ; or ~ character.]
        return ChainBranchUtils.getValidChainBranchName(strName);
    }

    public boolean canBuild() { return canBuild; }

    @Override
    public String toString() {
        return strName + " [" + strURI + "]";
    }
}
