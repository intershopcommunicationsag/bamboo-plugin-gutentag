package com.intershop.bamboo.plugins.gutentag;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.chains.ChainExecutionManager;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanHelper;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.branch.ChainBranch;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.bamboo.ww2.actions.ChainActionSupport;
import com.atlassian.bamboo.ww2.aware.ChainAware;
import com.atlassian.bamboo.ww2.aware.permissions.PlanReadSecurityAware;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.intershop.bamboo.plugins.gutentag.generic.RepositoryNotAvailableException;
import com.intershop.bamboo.plugins.gutentag.generic.TagBuildInfo;
import com.intershop.bamboo.plugins.gutentag.generic.VcsTag;

/**
 * Helps creating a tag build plan.
 *
 * Created by rpasold on 22.09.2015.
 */
@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class ManageTagsAction extends ChainActionSupport implements PlanReadSecurityAware, ChainAware {

    private static final long serialVersionUID = 4533823751853571360L;

    private static final Logger log = Logger.getLogger(ManageTagsAction.class);

    private final TagBuildManager tagBuildManager;
    private final TagBuildPersistenceManager tagBuildPersistenceManager;
    //private final I18nResolver i18nResolver;

    @Autowired
    public ManageTagsAction(@BambooImport ChainBranchManager chainBranchManager,
                            @BambooImport WebInterfaceManager webInterfaceManager,
                            @BambooImport BambooPermissionManager bambooPermissionManager,
                            @BambooImport PlanExecutionManager planExecutionManager,
                            @BambooImport PlanManager planManager,
                            @BambooImport BambooAuthenticationContext bambooAuthenticationContext,
                            @BambooImport CachedPlanManager cachedPlanManager,
                            @BambooImport BuildDefinitionManager buildDefinitionManager,
                            @BambooImport ChainExecutionManager chainExecutionManager,
                            TagBuildManager tagBuildManager,
                            TagBuildPersistenceManager tagBuildPersistenceManager
                            /*@BambooImport I18nResolver i18nResolver*/) {
        //this.i18nResolver = i18nResolver;
        super.setChainBranchManager(chainBranchManager);
        super.setWebInterfaceManager(webInterfaceManager);
        super.setBambooPermissionManager(bambooPermissionManager);
        super.setPlanExecutionManager(planExecutionManager);
        super.setPlanManager(planManager);
        super.setAuthenticationContext(bambooAuthenticationContext);
        super.setCachedPlanManager(cachedPlanManager);
        super.setBuildDefinitionManager(buildDefinitionManager);
        super.setChainExecutionManager(chainExecutionManager);
        this.tagBuildManager = tagBuildManager;
        this.tagBuildPersistenceManager = tagBuildPersistenceManager;
    }

    @Override
    public String execute() {
        //Always try to switch to root plan.
        if (getImmutablePlan().hasMaster()) {
            ImmutableChain chainMaster = PlanHelper.getMasterPlan(getImmutableChain());
            PlanKey planKeyMaster = chainMaster.getPlanKey();
            setPlan(chainMaster);
            setPlanKey(planKeyMaster.getKey());
            setChain(chainMaster);
          }
        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public String doBuild() {
        String strParentResult = execute();
        if (!strParentResult.equals(SUCCESS))
            return strParentResult;

        if (StringUtils.isBlank(strTagName))
            return ERROR;

        if (StringUtils.isBlank(strTagURI))
            return ERROR;

        VcsTag vcsTag = new VcsTag(strTagURI, strTagName, true);
        PlanRepositoryDefinition repositoryDefinitionOriginal = null;
        List<PlanRepositoryDefinition> listRepositoryDefinitions = getRepositoryDefinitions();
        for (PlanRepositoryDefinition repositoryDefinition : listRepositoryDefinitions) {
            if (repositoryDefinition.getId() == lRepoId) {
                repositoryDefinitionOriginal = repositoryDefinition;
                break;
            }
        }

        if (null == repositoryDefinitionOriginal)
            return ERROR;

        create(vcsTag, true);

        if (hasActionErrors() || hasAnyErrors()) {
            for (String s : getActionErrors()) {
                log.error(s);
            }
            return ERROR;
        }

        return "result";
    }

    @SuppressWarnings("unused")
    public String doDisable() {
        String strParentResult = execute();
        if (!strParentResult.equals(SUCCESS))
            return strParentResult;

        if (StringUtils.isBlank(strTagName))
            return ERROR;

        if (StringUtils.isBlank(strTagURI))
            return ERROR;

        if (getTypedPlanKey() != null)
            tagBuildManager.getTagBuildPersistenceManager().addTagForPlan(getTypedPlanKey(), new VcsTag(strTagName));
        else
            addActionError("No plan key available!");

        return getState();
    }

    @SuppressWarnings("unused")
    public String doDelete() {
        String strParentResult = execute();
        if (!strParentResult.equals(SUCCESS))
            return strParentResult;

        if (StringUtils.isBlank(planKeyToDelete))
            return ERROR;

        tagBuildManager.removeTagPlan(PlanKeys.getPlanKey(planKeyToDelete), getTypedPlanKey(), false);

        return getState();
    }

    @SuppressWarnings("unused")
    public String doDeleteFromHistory() {
        String strParentResult = execute();
        if (!strParentResult.equals(SUCCESS))
            return strParentResult;

        if (StringUtils.isBlank(strTagName))
            return ERROR;

        PlanKey planKey = getTypedPlanKey();
        if (planKey != null)
            tagBuildManager.getTagBuildPersistenceManager().removeTagForPlan(planKey, strTagName);

        return getState();
    }

    @NotNull
    private String getState()
    {
        if (hasActionErrors() || hasAnyErrors()) {
            for (String s : getActionErrors()) {
                log.error(s);
            }
            return ERROR;
        }

        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public String doInvalidate() {
        String strParentResult = execute();
        if (!strParentResult.equals(SUCCESS))
            return strParentResult;

        PlanKey planKey = getTypedPlanKey();
        if (planKey != null)
            tagBuildManager.getTagBuildPersistenceManager().forget(planKey);

        return getState();
    }


    public boolean hasPermission() {
        return bambooPermissionManager.hasPlanPermission(BambooPermission.BUILD, getImmutablePlan()) &&
                bambooPermissionManager.hasPlanPermission(BambooPermission.ADMINISTRATION, getImmutablePlan());
    }

    public List<PlanRepositoryDefinition> getRepositoryDefinitions() {
        return tagBuildManager.getPlanRepositoryDefinitions(getTypedPlanKey());
    }

    public boolean isPrimaryRepositoryDefinition(PlanRepositoryDefinition repositoryDefinition) {
        PlanRepositoryDefinition primaryPlanRepositoryDefinition = tagBuildManager.getPrimaryPlanRepositoryDefinition(getTypedPlanKey());
        return primaryPlanRepositoryDefinition!=null &&  primaryPlanRepositoryDefinition.equals(repositoryDefinition);
    }

    public List<VcsTag> getTagsForRepo(PlanRepositoryDefinition repositoryDefinition) {
        try {
            List<VcsTag> tagsForRepo = tagBuildManager.getTagsForRepo(repositoryDefinition);
            tagsForRepo.sort((o1, o2) -> o2.getName().compareTo(o1.getName()));
            return tagsForRepo;
        } catch (RepositoryNotAvailableException e) {
            log.error(e.getMessage());
            addActionError(e.getMessage());
            return Collections.emptyList();
        }
    }

    public List<TagBuildInfo> getRecordedTags() {
        PlanKey typedPlanKey = getTypedPlanKey();
        if (null == typedPlanKey)
            return Collections.emptyList();
        return tagBuildPersistenceManager.getTagsForPlan(typedPlanKey);
    }

    public Map<Date, List<TagBuildInfo>> getRecordedTagsByDay() {
        List<TagBuildInfo> tagsForPlan = getRecordedTags();
        if (tagsForPlan.isEmpty())
            return Collections.emptyMap();

        tagsForPlan.sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));
        Map<Date, List<TagBuildInfo>> mapTagsPerDate = new TreeMap<>(Comparator.reverseOrder());

        for (TagBuildInfo tagBuildInfo : tagsForPlan)
        {
            Date date = DateUtils.truncate(tagBuildInfo.getDate(), Calendar.DATE);
            if (!mapTagsPerDate.containsKey(date)) {
                List<TagBuildInfo> tagsOnDay = new ArrayList<>();
                mapTagsPerDate.put(date, tagsOnDay);
            }
            List<TagBuildInfo> tagsOnDay = mapTagsPerDate.get(date);
            tagsOnDay.add(tagBuildInfo);
        }

        return mapTagsPerDate;
    }

    private void create(VcsTag vcsTag, boolean doBuild) {
        final ImmutableChain chain = getImmutableChain();
        if (chain == null) {
            addActionError("Branch creation could not be performed, no plan found");
            return;
        }
        if (null == getUser()) {
            addActionError("No Permission (Unknown user)!");
            return;
        }

        if (doBuild) {
            PlanResultKey planResultKey = tagBuildManager.buildTagForPlan(vcsTag, chain.getPlanKey());
            if (null != planResultKey) {
                this.buildNumber = planResultKey.getBuildNumber();
            }
        }
        else
            tagBuildManager.createTagForPlan(vcsTag, chain.getPlanKey());
    }

    public boolean tagPlanExists(String strTagName){
        return tagBuildManager.tagPlanExists(strTagName, getTypedPlanKey());
    }

    public boolean isTagBuilt(String strTagName){
        return tagBuildManager.isTagBuilt(strTagName, getTypedPlanKey());
    }

    public boolean tagPlanActive(String planKey) {
        return chainExecutionManager.isActive(PlanKeys.getPlanKey(planKey));
    }

    public boolean tagPlanSuspended(String planKey) {
        Plan plan = planManager.getPlanByKey(PlanKeys.getPlanKey(planKey));
        return plan == null || plan.isSuspendedFromBuilding();
    }

    public ChainBranch getExistingPlan(VcsTag tag) {
        ImmutableChain immutableChain = getImmutableChain();
        if (immutableChain.hasMaster())
            immutableChain = immutableChain.getMaster();
        if (null == immutableChain)
            return null;
        List<ChainBranch> lstChainBranches = chainBranchManager.getBranchesForChain(immutableChain);
        for (ChainBranch chainBranch : lstChainBranches) {
            if (chainBranch.getBuildName().equals(tag.getPlanName()) || chainBranch.getName().equals(tag.getPlanName())) {
                return chainBranch;
            }
        }
        return null;
    }

    //public String getProperty(String key) {
    //   return i18nResolver.getText(key);
    //}

    private long lRepoId;

    @SuppressWarnings("unused") //via param
    public void setRepositoryId(long lRepoId) {
        this.lRepoId = lRepoId;
    }

    //public long getRepositoryId() {
    //    return this.lRepoId;
    //}

    private String strTagName;

    @SuppressWarnings("unused") //via param
    public void setTagName(String strTagName) {
        this.strTagName = strTagName;
    }

//    public String getTagName() {
//        return this.strTagName;
//    }

    private String strTagURI;

    @SuppressWarnings("unused") //via param
    public void setTagURI(String strTagURL) {
        this.strTagURI = strTagURL;
    }

//    public String getTagURI() {
//        return this.strTagURI;
//    }

    private String planKeyToDelete;

//    public String getPlanKeyToDelete() {
//        return planKeyToDelete;
//    }

    @SuppressWarnings("unused") //via param
    public void setPlanKeyToDelete(String planKeyToDelete) {
        this.planKeyToDelete = planKeyToDelete;
    }
}
