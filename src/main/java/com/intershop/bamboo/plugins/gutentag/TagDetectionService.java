package com.intershop.bamboo.plugins.gutentag;

import com.atlassian.bamboo.ServerLifecycleProvider;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.configuration.AdministrationConfigurationPersister;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@BambooComponent
public class TagDetectionService {
    private static final String KEY_GLOBAL_INTERVAL = "com.intershop.bamboo.plugins.gutentag:interval";
    private static final String KEY_GLOBAL_FLAG_DELETE_REMOVED = "com.intershop.bamboo.plugins.gutentag:deleteRemoved";
    private static final String KEY_GLOBAL_FLAG_DELETE_OLD = "com.intershop.bamboo.plugins.gutentag:deleteOld";
    private static final String KEY_GLOBAL_MAXAGE = "com.intershop.bamboo.plugins.gutentag:maxAge";

    @Autowired @BambooImport private ServerLifecycleProvider serverLifecycleProvider;
    @Autowired @BambooImport private AdministrationConfigurationAccessor administrationConfigurationAccessor;
    @Autowired @BambooImport private AdministrationConfigurationPersister administrationConfigurationPersister;
    @Autowired private TagBuildManager tagBuildManager;

    private TagDetector tagDetector;

    @PostConstruct
    private void postConstruct() {
        tagDetector = TagDetector.createInThread(loadInterval(),
                                                 loadDeleteRemoved(),
                                                 loadDeleteOld(),
                                                 loadMaxAge(),
                                                 serverLifecycleProvider,
                                                 tagBuildManager);
    }

    @PreDestroy
    private void preDestroy() {
        if (getTagDetector() != null)
            getTagDetector().requestThreadStop();
    }

    public TagDetector getTagDetector() {
        return tagDetector;
    }

    private long loadInterval() {
        final AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        String strInterval = administrationConfiguration.getSystemProperty(KEY_GLOBAL_INTERVAL);
        if (null!=strInterval)
            return Long.parseLong(strInterval);
        return TagDetector.DEFAULT_INTERVAL;
    }

    void saveInterval(long interval) {
        final AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        administrationConfiguration.setSystemProperty(KEY_GLOBAL_INTERVAL, String.valueOf(interval));
        administrationConfigurationPersister.saveAdministrationConfiguration(administrationConfiguration);
    }

    private boolean loadDeleteRemoved() {
        final AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        String strDeleteRemoved = administrationConfiguration.getSystemProperty(KEY_GLOBAL_FLAG_DELETE_REMOVED);
        if (null!=strDeleteRemoved)
            return Boolean.parseBoolean(strDeleteRemoved);
        return TagDetector.DEFAULT_FLAG_DELETE_REMOVED;
    }

    void saveDeleteRemoved(boolean deleteRemoved) {
        final AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        administrationConfiguration.setSystemProperty(KEY_GLOBAL_FLAG_DELETE_REMOVED, String.valueOf(deleteRemoved));
        administrationConfigurationPersister.saveAdministrationConfiguration(administrationConfiguration);
    }

    private boolean loadDeleteOld() {
        final AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        String strDeleteOld = administrationConfiguration.getSystemProperty(KEY_GLOBAL_FLAG_DELETE_OLD);
        if (null!=strDeleteOld)
            return Boolean.parseBoolean(strDeleteOld);
        return TagDetector.DEFAULT_FLAG_DELETE_OLD;
    }

    void saveDeleteOld(boolean deleteOld) {
        final AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        administrationConfiguration.setSystemProperty(KEY_GLOBAL_FLAG_DELETE_OLD, String.valueOf(deleteOld));
        administrationConfigurationPersister.saveAdministrationConfiguration(administrationConfiguration);
    }

    private int loadMaxAge() {
        final AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        String strMaxAge = administrationConfiguration.getSystemProperty(KEY_GLOBAL_MAXAGE);
        if (null!=strMaxAge)
            return Integer.parseInt(strMaxAge);
        return TagDetector.DEFAULT_MAXAGE_DAYS;
    }

    void saveMaxAge(int maxAge) {
        final AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        administrationConfiguration.setSystemProperty(KEY_GLOBAL_MAXAGE, String.valueOf(maxAge));
        administrationConfigurationPersister.saveAdministrationConfiguration(administrationConfiguration);
    }
}
