package com.intershop.bamboo.plugins.gutentag;

import com.atlassian.bamboo.event.BuildConfigurationUpdatedEvent;
import com.atlassian.bamboo.event.ChainDeletedEvent;
import com.atlassian.bamboo.event.ServerStartedEvent;
import com.atlassian.event.api.EventListener;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TagDetectionEventListener {
    private static final Logger log = Logger.getLogger(TagDetectionEventListener.class);

    @Autowired
    private TagBuildPersistenceManager tagBuildPersistenceManager;

    @EventListener
    public void onBuildConfigurationUpdated(final BuildConfigurationUpdatedEvent event) {
        log.info("Caught " + event.getClass().getSimpleName() + " for " + event.getPlanKey());
        tagBuildPersistenceManager.invalidateCache();
    }

    @EventListener
    public void onChainDeletedEvent(final ChainDeletedEvent event) {
        log.info("Caught " + event.getClass().getSimpleName() + " for " + event.getPlanKey());
        tagBuildPersistenceManager.invalidateCache();
    }

    @EventListener
    public void onServerStartedEvent(final ServerStartedEvent event) {
        log.info("Caught " + event.getClass().getSimpleName());
    }
}
