package com.intershop.bamboo.plugins.gutentag;

import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.trigger.PlanTrigger;
import com.atlassian.bamboo.plan.trigger.PlanTriggerResult;
import com.atlassian.bamboo.plan.trigger.PlanTriggerResultBuilder;
import com.atlassian.bamboo.plan.vcsRevision.PlanVcsRevisionDataSet;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.v2.build.BuildChanges;
import com.atlassian.bamboo.v2.build.trigger.CustomRevisionBuildTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.atlassian.bamboo.v2.trigger.ChangeDetectionManager;
import com.atlassian.bamboo.v2.trigger.ManualBuildDetectionAction;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class TagDetectionPlanTrigger implements PlanTrigger {
    private static final Logger log = Logger.getLogger(TagDetectionPlanTrigger.class);

    private final CachedPlanManager cachedPlanManager;
    private final ChangeDetectionManager changeDetectionManager;

    @Autowired
    public TagDetectionPlanTrigger(@BambooImport CachedPlanManager cachedPlanManager,
                            @BambooImport ChangeDetectionManager changeDetectionManager) {
        this.cachedPlanManager = cachedPlanManager;
        this.changeDetectionManager = changeDetectionManager;
    }

    @NotNull
    @Override
    public PlanTriggerResult triggerPlan(@NotNull TriggerReason triggerReason, @NotNull PlanResultKey planResultKey, @NotNull Map<String, String> params, @NotNull Map<String, String> customVariables, @Nullable PlanVcsRevisionDataSet lastVcsRevisionKeys) {

        final PlanTriggerResultBuilder resultBuilder = PlanTriggerResultBuilder.create(customVariables);

        try {
            final ImmutableChain chain = cachedPlanManager.getPlanByKeyIfOfType(planResultKey.getPlanKey(), ImmutableChain.class);
            if (chain == null) {
                throw new IllegalStateException("Could not trigger a build with reason '" + triggerReason.getName() + "' because the plan '" + planResultKey.getPlanKey() + "' does not exist");
            }

            String revision = null;
            CustomRevisionBuildTriggerReason customRevisionBuildTriggerReason = Narrow.to(triggerReason, CustomRevisionBuildTriggerReason.class);
            if (customRevisionBuildTriggerReason != null) {
                resultBuilder.addVariable(ManualBuildDetectionAction.CUSTOM_REVISION_PARAMETER, customRevisionBuildTriggerReason.getRevision());
                revision = customRevisionBuildTriggerReason.getRevision();
            }

            BuildChanges buildChanges = changeDetectionManager.collectAllChangesSinceLastBuild(chain, customVariables, revision);
            resultBuilder.addBuildChanges(buildChanges);
        } catch (RepositoryException e) {
            final String errorMessage = "Could not detect changes for plan '" + planResultKey + "' with trigger '" + triggerReason.getName() + "'";
            log.error(errorMessage, e);
            resultBuilder.addErrorMessage(errorMessage, e);
        }

        return resultBuilder.build();
    }

    @NotNull
    @Override
    public Map<String, String> getVariablesForContinuedBuild(@NotNull TriggerReason triggerReason) {
        return new HashMap<>();
    }
}
