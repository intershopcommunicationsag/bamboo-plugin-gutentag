package com.intershop.bamboo.plugins.gutentag.generic;

public class RepositoryNotAvailableException extends Exception {
    public RepositoryNotAvailableException(String identifier, String cause) {
        super("Repsistory '" + identifier + "' not available! Cause: '" + cause + "'");
    }
}
