package com.intershop.bamboo.plugins.gutentag;

import com.atlassian.bamboo.plan.PlanKey;
import com.google.common.collect.Maps;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Map;

class TagDetectionConfiguration implements Serializable {

    private static final long serialVersionUID = 1716252810459788664L;

    static final String DEFAULT_TAG_MATCH_NAME_PATTERN = ".*";
    private static final String CONFIG_KEY_TAG_CREATION = "custom.gutentag.tagCreationEnabled";
    private static final String CONFIG_KEY_TAG_MATCHING_PATTERN = "custom.gutentag.matchingPattern";

    private boolean isTagCreationEnabled;
    private String planTagCreationPattern = ".*";
    private final PlanKey planKey;

    TagDetectionConfiguration(@NotNull Map<String, String> config, PlanKey planKey) {
        setPlanTagCreationEnabled(Boolean.parseBoolean(config.getOrDefault(CONFIG_KEY_TAG_CREATION, "false")));
        setMatchingPattern(config.getOrDefault(CONFIG_KEY_TAG_MATCHING_PATTERN, DEFAULT_TAG_MATCH_NAME_PATTERN));
        this.planKey = planKey;
    }

    @NotNull
    Map<String, String> toConfiguration() {
        Map<String, String> config = Maps.newHashMap();

        config.put(CONFIG_KEY_TAG_CREATION, String.valueOf(isPlanTagCreationEnabled()));
        config.put(CONFIG_KEY_TAG_MATCHING_PATTERN, getMatchingPattern());

        return config;
    }

    boolean isPlanTagCreationEnabled() {
        return isTagCreationEnabled;
    }

    void setPlanTagCreationEnabled(boolean isTagCreationEnabled) {
        this.isTagCreationEnabled = isTagCreationEnabled;
    }

    String getMatchingPattern() {
        return planTagCreationPattern;
    }

    void setMatchingPattern(@NotNull String regularExpression) {
        this.planTagCreationPattern = regularExpression;
    }

    public PlanKey getPlanKey() {
        return planKey;
    }
}
