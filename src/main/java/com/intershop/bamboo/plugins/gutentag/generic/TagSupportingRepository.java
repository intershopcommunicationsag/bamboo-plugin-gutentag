package com.intershop.bamboo.plugins.gutentag.generic;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Shared interface for repositories supporting tags.
 *
 * Created by rpasold on 30.09.2015.
 */
public interface TagSupportingRepository {
    @NotNull
    List<VcsTag> getTags() throws RepositoryNotAvailableException;

    boolean isTagBuildSupported();
}
