package com.intershop.bamboo.plugins.gutentag;

public enum TagCreationType {
    DISABLED("disabled"),
    ALL_NEW("all"),
    NEW_MATCHES("match");

    private final String key;

    TagCreationType(String key)
    {
        this.key = key;
    }

    public String getKey()
    {
        return this.key;
    }
}
