package com.intershop.bamboo.plugins.gutentag.git.ssh;

import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class SshCredentialProvider extends UsernamePasswordCredentialsProvider {

    public SshCredentialProvider(String password) {
        super("nothing", password);
    }

    @Override
    public boolean get(URIish uri, CredentialItem... items) throws UnsupportedCredentialItem {
        if (items.length == 1) {
            CredentialItem credentialItem = items[0];
            if (credentialItem instanceof CredentialItem.StringType) {
                CredentialItem.StringType input = ((CredentialItem.StringType) credentialItem);
                if (credentialItem.isValueSecure()){
                    CredentialItem.Password password = new CredentialItem.Password();
                    super.get(uri, password);
                    input.setValue(String.valueOf(password.getValue()));
                    return true;
                }
            }
            else if (credentialItem instanceof CredentialItem.YesNoType) { //Verification of host.
                CredentialItem.YesNoType input = ((CredentialItem.YesNoType) credentialItem);
                input.setValue(true);
                return true;
            }
        }
        return super.get(uri, items);
    }
}
