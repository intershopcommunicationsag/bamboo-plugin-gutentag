package com.intershop.bamboo.plugins.gutentag.svn;

import com.atlassian.bamboo.plan.branch.VcsBranch;
import com.atlassian.bamboo.repository.AuthenticationType;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.repository.svn.SvnRepository;
import com.atlassian.bamboo.repository.svn.v2.configurator.SvnConfigurationConstants;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.collect.Lists;
import com.intershop.bamboo.plugins.gutentag.generic.TagSupportingRepository;
import com.intershop.bamboo.plugins.gutentag.generic.VcsTag;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

/**
 * A wrapper for the builtin SvnRepository with added support for Tags.
 * We're using a different base path for branches as in SVN a directory is a directory,
 * there is no actual difference between branches and tags.
 *
 * Created by rpasold on 29.09.2015.
 */
public class TagSupportingSvnRepository implements TagSupportingRepository {
    private static final String TAGS = "/tags";

    private final SvnRepository svnRepository;

    public TagSupportingSvnRepository(PlanRepositoryDefinition repositoryDefinition) {
        this.svnRepository = new SvnRepository();
        ContainerManager.autowireComponent(svnRepository);
        Map<String, String> configuration = repositoryDefinition.getVcsLocation().getConfiguration();
        String repositoryUrl = configuration.get(SvnConfigurationConstants.SVN_REPO_ROOT_URL);
        svnRepository.setRepositoryUrl(repositoryUrl);
        String authType = configuration.get(SvnConfigurationConstants.SVN_AUTH_TYPE);
        svnRepository.setAuthType(authType);
        svnRepository.setUsername(configuration.get(SvnConfigurationConstants.SVN_USERNAME));
        svnRepository.setUseExternals(Boolean.valueOf(configuration.get(SvnConfigurationConstants.USE_EXTERNALS)));
        svnRepository.setUserPassword(configuration.get(SvnConfigurationConstants.SVN_PASSWORD));

        if (authType.equals(AuthenticationType.PASSWORD.getKey())) {
            svnRepository.setUserPassword(configuration.get(SvnConfigurationConstants.SVN_PASSWORD));
        }
        else if (authType.equals(AuthenticationType.SSH.getKey())) {
            svnRepository.setKeyFile(configuration.get(SvnConfigurationConstants.SVN_KEYFILE));
            svnRepository.setEncryptedPassphrase(configuration.get(SvnConfigurationConstants.SVN_PASSPHRASE));
        }
        else if (authType.equals(AuthenticationType.SSL_CLIENT_CERTIFICATE.getKey())) {
            svnRepository.setKeyFile(configuration.get(SvnConfigurationConstants.SVN_SSL_KEYFILE));
            svnRepository.setEncryptedPassphrase(configuration.get(SvnConfigurationConstants.SVN_SSL_PASSPHRASE));
        }
        svnRepository.setAutodetectBranchRoot(false);
        svnRepository.setManualBranchRootUrl(repositoryUrl + TAGS);
    }

    @Override
    @NotNull
    public List<VcsTag> getTags() {
        final List<VcsTag> tags = Lists.newArrayList();
        try {
            List<VcsBranch> branches = svnRepository.getOpenBranches(null);
            for (VcsBranch branch : branches) {
                VcsTag vcsTag = new VcsTag(TAGS + "/" + branch.getName(), branch.getDisplayName(), isTagBuildSupported());
                tags.add(vcsTag);
            }
        }
        catch (RepositoryException ignored) {}
        return tags;
    }

    @Override
    public boolean isTagBuildSupported() {
        return true;
    }
}
