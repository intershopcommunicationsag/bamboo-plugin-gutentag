[#-- @ftlvariable name="action" type="com.intershop.bamboo.plugins.gutentag.ManageTagsAction" --]

[#assign hasPermission = action.hasPermission()]
  [#list action.repositoryDefinitions! as repositoryDefinition]
      [#assign tagsForRepo = action.getTagsForRepo(repositoryDefinition)!]
  <table class="aui aui-zebra" id="dashboard" style="max-width:500px;margin:15px;">
    <colgroup>
      <col style="min-width: 200px;">
      <col style="width: 91px">
    </colgroup>
    <thead>
      <tr>
          <th><a href="${req.contextPath}/chain/admin/config/editRepository.action?planKey=${plan.key}&repositoryId=${repositoryDefinition.id}">${repositoryDefinition.name}</a><small><sup>[${tagsForRepo.size()}]</sup></small></th>
        <th></th>
      </tr>
    </thead>
    <tbody class="project" data-project-id="${plan.id}">
      [#list tagsForRepo as tag]
      <tr>
        [#if action.tagPlanExists(tag.getPlanName())]
          [#assign tagPlanKey = action.getExistingPlan(tag).getPlanKey().getKey()]
          <td class="build">
            <a id="changeTag-${tag.name}" class="mutative" href="${req.contextPath}/browse/${tagPlanKey}">${tag.name}</a>
          </td>
          <td class="dashboard-operations">
            [#if hasPermission]
              [#if action.tagPlanSuspended(tagPlanKey)]
              <a class="enableBuild usePostMethod" id="resumeBuild_${tagPlanKey}" data-plan-key="${tagPlanKey}" href="${req.contextPath}/build/admin/resumeBuild.action?buildKey=${tagPlanKey}&amp;returnUrl=%2Fstart.action&amp;mark=CCCCCCCCC">
                <span class="icon icon-build-enable" title="Enable '${tag.name}'">
                  <span>Enable '{tag.name}'</span>
                </span>
              </a>
              [/#if]
              [#if action.tagPlanActive(tagPlanKey)]
              <a class="asynchronous usePostMethod" id="stopSingleBuild_${tagPlanKey}" href="${req.contextPath}/build/admin/ajax/stopPlan.action?planKey=${tagPlanKey}">
                <span class="icon icon-build-stop" title="Stop '${tag.name}'">
                  <span>Stop '${tag.name}'</span>
                </span>
              </a>
              [#else]
               <a id="deletePlan:${tagPlanKey}" class="item-link plan-delete-link" href="${req.contextPath}/build/gutenTagReset!delete.action?planKey=${plan.key}&planKeyToDelete=${tagPlanKey}">
                <span class="aui-icon aui-icon-small aui-iconfont-remove" title="Exterminate '${tag.name}'">
                  <span>Exterminate '${tag.name}'</span>
                </span>
              </a>
              [/#if]
              <a id="editBuild:${tagPlanKey}" href="${req.contextPath}/chain/admin/config/editChainConfiguration.action?buildKey=${tagPlanKey}">
                <span class="aui-icon aui-icon-small aui-iconfont-edit" title="Edit '${tag.name}'">Edit '${tag.name}'</span>
              </a>
              <a style="display:none;" class="markBuildFavourite usePostMethod" id="toggleFavourite_${tagPlanKey}" href="${req.contextPath}/build/label/setFavourite.action?newFavStatus=true&amp;planKey=${tagPlanKey}&amp;returnUrl=%2Fchain%2FviewChainBranches.action%3FplanKey%3D${plan.key}" data-plan-key="${tagPlanKey}" title="Favourite branch">
                <span class="aui-icon aui-icon-small aui-iconfont-unstar" title="Favourite branch">Favourite branch</span>
              </a>
            [/#if]
          </td>
        [#elseif action.isTagBuilt(tag.getName())]
          <td class="build">
            <strong>${tag.name}</strong>
          </td>
          <td class="dashboard-operations">
          [#if hasPermission]
            <a id="deletePlan:${tag.name}" class="item-link plan-delete-link" href="${req.contextPath}/build/gutenTagReset!deleteFromHistory.action?planKey=${plan.key}&tagName=${tag.name}">
              <span class="aui-icon aui-icon-small aui-iconfont-remove" title="Exterminate '${tag.name}'">
                <span>Exterminate '${tag.name}'</span>
              </span>
            </a>
          [/#if]
        [#elseif hasPermission]
         <td class="build">
          <strong>${tag.name}</strong>
         </td>
         <td class="dashboard-operations">
           [#if tag.canBuild() && action.isPrimaryRepositoryDefinition(repositoryDefinition)]
           <a class="asynchronous usePostMethod" id="changeTag-${tag.name}" href="${req.contextPath}/build/gutenTag!build.action?planKey=${plan.key}&repositoryId=${repositoryDefinition.id}&tagName=${tag.name}&tagURI=${tag.uri?url}">
             <span class="icon icon-build-run" title="Run">
               <span>Run</span>
             </span>
           </a>
           <a class="asynchronous usePostMethod" id="disableTag-${tag.name}" href="${req.contextPath}/build/gutenTag!disable.action?planKey=${plan.key}&repositoryId=${repositoryDefinition.id}&tagName=${tag.name}&tagURI=${tag.uri?url}">
             <span class="aui-icon aui-icon-close" title="Disable (mark as seen)">
               <span>Run</span>
             </span>
           </a>
           [#else]
             [#if !tag.canBuild()]
               [#assign lockedReason = "Cannot build ${tag.name}"]
             [#else]
               [#assign lockedReason = "Not primary repository"]
             [/#if]
           <span class="aui-icon aui-icon-small aui-iconfont-locked" title="${lockedReason}">
             <span>${lockedReason}</span>
           </span>
           [/#if]
         </td>
        [#else]
          <td class="build">
            ${tag.name}
          </td>
          <td class="dashboard-operations">
            <span class="aui-icon aui-icon-small aui-iconfont-locked" title="No permission">
              <span>No permission</span>
            </span>
          </td>
        [/#if]
      </tr>
      [/#list]
      </tbody>
    </table>
  [/#list]
  [#if action.hasActionErrors()]
    [#assign errors = action.getActionErrors()]
    [#list errors! as error]
      [@ui.messageBox type='error']
         <p>${error}</p>
      [/@ui.messageBox]
    [/#list]
  [/#if]
