[#-- @ftlvariable name="action" type="com.intershop.bamboo.plugins.gutentag.ConfigureTagDetectionAdminAction" --]
[#-- @ftlvariable name="" type="com.intershop.bamboo.plugins.gutentag.ConfigureTagDetectionAdminAction" --]

<html lang="en">
    [#--noinspection HtmlRequiredTitleElement--]
    <head>
        [@ui.header pageKey="gutentag.plugin.name" title=true /]
        <meta name="decorator" content="adminpage"/>
    </head>
    <body>
    <h1>${action.getText("gutentag.plugin.name")}</h1>
    <p>${action.getText("gutentag.plugin.description")}</p>
    <h2>Configuration</h2>
    [@ww.form id='configureTagDetection' action='configureTagDetectionAdmin!save' submitLabelKey='global.buttons.saveAndApply']
        [@ui.bambooSection titleKey='tagdetection.name' ]
            <p>[@ww.text name="tagdetection.interval.description" /]</p>
            [@ww.textfield labelKey="tagdetection.interval.name" name="interval" required='true'/]
        [/@ui.bambooSection]
        [@ui.bambooSection titleKey='tagdetection.removal.name' ]
            <p>[@ww.text name="tagdetection.flagdeleteremoved.description" /]</p>
            [@ww.checkbox name="deleteRemoved" labelKey="tagdetection.flagdeleteremoved.name" toggle=true/]
            [@ww.checkbox name="deleteOld" labelKey="tagdetection.flagdeleteold.name" toggle=true/]
            [@ui.bambooSection dependsOn='deleteOld' showOn='true']
                [@ww.textfield labelKey='tagdetection.flagdeleteold.maxage.name' name='maxAge'/]
            [/@ui.bambooSection]
        [/@ui.bambooSection]
    [/@ww.form]
    </body>
</html>
