[#-- @ftlvariable name="action" type="com.intershop.bamboo.plugins.gutentag.ManageTagsAction" --]
<html lang="en">
<head>
    [@ui.header pageKey='gutentag.label' object=immutableChain.name title=true /]
    <meta name="tab" content="tags"/>
    <!-- The following is taken from /bamboo-web-app/src/main/webapp/fragments/plan/displayWideBuildPlansList.ftl -->
    <script type="text/javascript">
        BAMBOO.XsrfUtils.registerLinkClickHandler();
        [#-- initiate async manager for operations (handles e.g. inline stopping/starting of builds) --]
        jQuery(function() {
            console.log("initiate async manager for operations (handles e.g. inline stopping/starting of builds)");
            AsynchronousRequestManager.init(".asynchronous", null, function (json) {
                if (json !== undefined && json.status !== undefined && json.status === "ERROR") {
                    var $message = AJS.$("<div/>").addClass("error-holder"),
                        $messageContainer = AJS.$("#ajaxErrorHolder"); // main Bamboo dashboard

                    AJS.messages["error"]($message, { closeable: true, body: json.errors.join(" ") });
                    $message.delay(5000).fadeOut(function () {
                        AJS.$(this).remove();
                    });
                    if ($messageContainer.length === 0) {
                        $messageContainer = AJS.$("#dashboard"); // project dashboard
                        $message.insertBefore($messageContainer);
                    } else {
                        $message.insertAfter($messageContainer);
                    }
                }
                location.reload();
            });
        });
    </script>
</head>
<body>
[#if !action.returnUrl?has_content]
    [@ui.header pageKey='gutentag.label' id='planTagTitle' /]
    <p>${action.getText("gutentag.action.description", "All tags per plan repository")}</p>

    <div style="float: right;background: #f5f5f5;padding: 1em;margin-right: 2em;">
        [#assign tagBuildInfoList = action.recordedTags]
        <h2>${action.getText("gutentag.action.recorded", "Recorded tags")}
            <small><sup>[${tagBuildInfoList.size()}]</sup></small>
        </h2>
        <ul>
            [#assign tagBuildInfoPerDay = action.recordedTagsByDay]
            [#list tagBuildInfoPerDay.keySet() as dateOfDay]
                <li><strong>${dateOfDay?date}</strong>
                    <ul>
                        [#list tagBuildInfoPerDay.get(dateOfDay) as tagBuildInfo]
                            <li>
                                [#if tagBuildInfo.planKey??]
                                    <a id="recordedTag-${tagBuildInfo.name}" class="mutative" href="${req.contextPath}/browse/${tagBuildInfo.planKey}">${tagBuildInfo.name}</a>
                                [#else]
                                    ${tagBuildInfo.name}
                                [/#if]
                            </li>
                        [/#list]
                    </ul>
                </li>
            [/#list]
        </ul>
    </div>
    [#include "listTagsForPlan.ftl"]
[#else]
    [@s.form cancelUri='${action.returnUrl}']
        [#include "listTagsForPlan.ftl"]
    [/@s.form]
[/#if]
</body>
</html>
