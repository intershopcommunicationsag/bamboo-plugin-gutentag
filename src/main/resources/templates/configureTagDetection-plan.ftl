[#-- @ftlvariable name="action" type="com.intershop.bamboo.plugins.gutentag.ConfigureTagDetectionPlanAction" --]
[#--noinspection FtlFileReferencesInspection--]
[#-- @ftlvariable name="" type="com.intershop.bamboo.plugins.gutentag.ConfigureTagDetectionPlanAction" --]
[#import "../chain/edit/editChainConfigurationCommon.ftl" as eccc/]
[#import "/lib/chains.ftl" as cn]

[#assign buttonsToolbar]
    [#if fn.hasPlanPermission('ADMINISTRATION', immutablePlan)]
        [@ww.url var='createTagUrl' action='gutenTag' namespace='/build' planKey=planKey returnUrl=currentUrl/]
        [@cp.displayLinkButton buttonId='createTag' buttonLabel='tag.create.new.title' buttonUrl=createTagUrl/]

        <script type="text/javascript">
            BAMBOO.redirectToTagRepositoryConfig = function (data) {
                window.location = AJS.contextPath() + data.redirectUrl;
            }
        </script>
        [@dj.simpleDialogForm triggerSelector="#createTag" width=520 height=500 headerKey="tag.create.new.header" submitCallback="BAMBOO.redirectToTagRepositoryConfig" /]
    [/#if]
[/#assign]

[@eccc.editChainConfigurationPage plan=immutablePlan selectedTab='chain.tag' titleKey='gutentag.label' descriptionKey='tagdetection.description' tools=buttonsToolbar ]
    [#if !fn.hasPlanPermission('ADMINISTRATION', immutablePlan)]
        [@ui.messageBox type="warning" titleKey='chain.config.branches.permission.error' /]
    [#else]
        [@ww.form action='configureTagDetectionPlan!update'
            namespace='/chain/admin/config'
            showActionErrors='false'
            id='chain.tag'
            cancelUri='/browse/${immutablePlan.key}/config'
            submitLabelKey='global.buttons.update']

            [@ww.hidden name="planKey" /]
            [#if action.tagDetectionCapable]

                [@ui.bambooSection titleKey='tagdetection.plan.automatic.management.title' descriptionKey='tagdetection.plan.automatic.management.description']

                    [@ww.select
                        key='tagdetection.plan.creation.title'
                        name='planTagCreation'
                        toggle='true'
                        list=tagCreationTypes
                        i18nPrefixForValue='tagdetection.plan.creation.select'
                        listValue='key'
                        listKey='key'
                        longField=true
                        helpDialogKey='tagdetection.plan.creation.helpdialog'/]

                    [@ui.bambooSection dependsOn='planTagCreation' showOn='match']
                        [@ww.textfield labelKey='tagdetection.plan.creation.regular.expression' name='planTagCreationRegularExpression' longField=true/]
                        [@ww.checkbox key="tagdetection.plan.creation.option.newonly" name="newOnly" labelKey="tagdetection.plan.creation.option.newonly.description" toggle=true/]
                    [/@ui.bambooSection]

                    [@ui.bambooSection dependsOn='planTagCreation' showOn='all']
                        [@ww.checkbox key="tagdetection.plan.creation.option.newonly" name="newOnly" labelKey="tagdetection.plan.creation.option.newonly.description" toggle=true/]
                    [/@ui.bambooSection]

                [/@ui.bambooSection]
            [#else]
                [@ui.bambooPanel titleKey='tagdetection.plan.automatic.management.title' description=automaticallyCreateTags headerWeight='h3']
                    [@ui.messageBox titleKey='tagdetection.plan.error']
                    <p>[@ww.text name='tagdetection.plan.error.message' /]</p>
                    [/@ui.messageBox]
                [/@ui.bambooPanel]
            [/#if]
        [/@ww.form]
    [/#if]
[/@eccc.editChainConfigurationPage]
