Atlassian BAMBOO plugin gutentag
================================

Originally Bamboo is not able to build from tags (for both SVN, Git).
This plugin brings the needed functionality into Bamboo.